-- Start contract notification trigger --

CREATE TRIGGER add_notification_contract AFTER INSERT ON `contracts` FOR EACH ROW
BEGIN
    INSERT INTO `notifications` (`notification_id`, `notification_type`, `id_user`, `id_sender`,`created_at`, `updated_at`)
    VALUES (NEW.id, 'contract', NEW.id_customer, NEW.id_vendor, NOW(), NOW());
END

-- End contract notification trigger --

-- Start contract installation notification trigger --

CREATE TRIGGER add_notification_contract_detail AFTER INSERT ON `billboard_contract_details` FOR EACH ROW
BEGIN
    INSERT INTO `notifications` (`notification_id`, `notification_type`, `id_user`, `id_sender`,`created_at`, `updated_at`)
    VALUES (NEW.id_contract, 'billboard_contract', (SELECT `id_customer` FROM `contracts` WHERE id = NEW.id_contract LIMIT 1), (SELECT `id_vendor` FROM `contracts` WHERE id = NEW.id_contract LIMIT 1),NOW(), NOW());
END

-- End contract installation notification trigger --

-- Start new billboard notification trigger --

CREATE TRIGGER add_notification_billboard AFTER INSERT ON `billboards` FOR EACH ROW
BEGIN
    INSERT INTO `notifications` (`notification_id`, `notification_type`, `id_user`, `created_at`, `updated_at`)
    VALUES (NEW.id, 'billboard', NEW.id_user, NOW(), NOW());
END

-- End new billboard notification trigger --

-- Start new feedback notification trigger --

CREATE TRIGGER add_notification_feedback AFTER INSERT ON `tickets` FOR EACH ROW
BEGIN
    INSERT INTO `notifications` (`notification_id`, `notification_type`, `id_user`, `id_sender`,`created_at`, `updated_at`)
    VALUES ( (SELECT `id_contract` FROM `billboard_contract_details` WHERE id = NEW.id_detail_contract), 'ticket', (
        SELECT `id_vendor` FROM `contracts` WHERE id = (
            SELECT `id_contract` FROM `billboard_contract_details` WHERE id = NEW.id_detail_contract
        )
    ), (
        SELECT `id_customer` FROM `contracts` WHERE id = (
            SELECT `id_contract` FROM `billboard_contract_details` WHERE id = NEW.id_detail_contract
        )
    ),NOW(), NOW());
END

-- End new feedback notification trigger --

-- Start new feedback notification trigger --

CREATE TRIGGER add_notification_feedback_reply AFTER INSERT ON `ticket_contents` FOR EACH ROW
BEGIN
    CALL feedback_reply_notification(NEW.id_user, NEW.id_ticket);
END

-- End new feedback notification trigger --

-- Start approval billboard notification trigger --

CREATE TRIGGER `approval_billboard` AFTER UPDATE ON `billboards`
 FOR EACH ROW BEGIN
	CALL billboard_approval_notification(NEW.id, OLD.verify, NEW.verify, NEW.id_user);          
END

-- End approval billboard notification trigger --