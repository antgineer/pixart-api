-- Start feedback reply notification SP --

DELIMITER $$
CREATE DEFINER=`root`@`localhost` PROCEDURE `feedback_reply_notification`(IN `idUser` INT UNSIGNED, IN `idTicket` INT UNSIGNED)
    NO SQL
BEGIN

DECLARE role varchar(20);
DECLARE idUserNotif int unsigned;
DECLARE idSenderNotif int unsigned;
DECLARE idNotif int unsigned;

SELECT role_name INTO role FROM `users`
INNER JOIN roles ON users.id_role = roles.id
WHERE users.id = idUser;

SELECT `id_contract` INTO idNotif FROM `billboard_contract_details` WHERE id = (
    SELECT id_detail_contract FROM `tickets` WHERE id = idTicket
);

IF(role != 'vendor') THEN
	SELECT `id_vendor`, `id_customer` INTO idUserNotif, idSenderNotif FROM `contracts` WHERE id = (
        SELECT `id_contract` FROM `billboard_contract_details` WHERE id = (
            SELECT id_detail_contract FROM `tickets` WHERE id = idTicket
        ) 
    );
ELSE 
	SELECT `id_customer`, `id_vendor` INTO idUserNotif, idSenderNotif FROM `contracts` WHERE id = (
        SELECT `id_contract` FROM `billboard_contract_details` WHERE id = (
            SELECT id_detail_contract FROM `tickets` WHERE id = idTicket
        ) 
    );
END IF;

INSERT INTO `notifications` (`notification_id`, `notification_type`, `id_user`, `id_sender`, `created_at`, `updated_at`)
VALUES (idNotif, 'ticket_content', idUserNotif, idSenderNotif, NOW(), NOW());

END$$
DELIMITER ;

-- End feedback reply notification SP --

-- Start approval billboard notification SP --

DELIMITER $$
CREATE DEFINER=`root`@`localhost` PROCEDURE `billboard_approval_notification`(IN `idBillboard` INT UNSIGNED, IN `oldStatus` VARCHAR(1), IN `newStatus` VARCHAR(1), IN `idVendor` INT UNSIGNED)
    NO SQL
BEGIN

DECLARE notifType varchar(30);

IF(oldStatus != newStatus) THEN

	IF(newStatus = 1) THEN
    	SET notifType = 'billboard_approved';
    ELSE
    	SET notifType = 'billboard_declined';
    END IF;
    
    INSERT INTO notifications (notification_id, notification_type, id_user, created_at, updated_at) 
    VALUES (idBillboard, notifType, idVendor, NOW(), NOW());
    
END IF;

END$$
DELIMITER ;

-- End approval billboard notification SP --