-- Urut ya updatenya, dari paling atas --

-- BATCH 1 --

-- isi field nama_provinsi_gmap sama dengan nama_provinsi --
UPDATE provinsis SET nama_provinsi_gmap = nama_provinsi;

-- isi field nama_kabupaten_gmap sama dengan nama_kabupaten --
UPDATE kabupatens SET nama_kabupaten_gmap = nama_kabupaten;

-- isi field nama_kecamatan_gmap sama dengan nama_kecamatan --
UPDATE kecamatans SET nama_kecamatan_gmap = nama_kecamatan;

-- Ganti nama provinsi DKI Jakarta --
UPDATE provinsis SET nama_provinsi_gmap = 'Daerah Khusus Ibukota Jakarta' WHERE id = 31;

-- Ganti nama provinsi DI Yogyakarta --
UPDATE provinsis SET nama_provinsi_gmap = 'Daerah Istimewa Yogyakarta' WHERE id = 34;

-- Hilangkan 'ADM' dari provinsi Daerah Khusus Ibukota Jakarta --
UPDATE kabupatens SET nama_kabupaten_gmap = REPLACE(nama_kabupaten_gmap, 'ADM. ', '');

-- Ganti 'KAB.' dengan 'KABUPATEN' --
UPDATE kabupatens SET nama_kabupaten_gmap = REPLACE(nama_kabupaten_gmap, 'KAB.', 'KABUPATEN');

-- Ganti 'KEP.' dengan 'KEPULAUAN' di Kabupaten --
UPDATE kabupatens SET nama_kabupaten_gmap = REPLACE(nama_kabupaten_gmap, 'KEP.', 'KEPULAUAN');

-- Ganti nama 'Kepulauan Seribu Selatan' --
UPDATE kecamatans SET nama_kecamatan_gmap = 'Kepulauan Seribu Selatan' WHERE id = 310102;

-- Ganti 'KEP.' dengan 'KEPULAUAN' di Kecamatan --
UPDATE kecamatans SET nama_kecamatan_gmap = REPLACE(nama_kecamatan_gmap, 'Kep.', 'Kepulauan');

-- Ganti nama 'Sei Rampah' --
UPDATE kecamatans SET nama_kecamatan_gmap = 'Sei Rampah' WHERE id = 121804;

-- Ganti nama 'Sei Rampah' --
UPDATE kecamatans SET nama_kecamatan_gmap = 'Kecamatan Setiabudi' WHERE id = 317402;

-- END BATCH 1 --