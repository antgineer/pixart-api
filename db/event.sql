-- Start Event detail kontrak expired --

CREATE DEFINER=`root`@`localhost` EVENT `update_status_installation_and_notification` 
ON SCHEDULE EVERY 1 DAY 
STARTS '2017-09-26 00:00:00' ON COMPLETION PRESERVE ENABLE 
COMMENT 'update status installasi ketika expired & beri notifikasi' 
DO BEGIN

    INSERT INTO notifications (`notification_id`, `notification_type`, `id_user`, `id_sender`, `created_at`, `updated_at`) 
    SELECT id_contract, 'billboard_contract_expired', (SELECT id_customer FROM contracts WHERE id = bcd.id_contract), id_user,NOW(), NOW() 
    FROM billboard_contract_details AS bcd WHERE expired_date = CURDATE() - INTERVAL 1 DAY AND status = 1;

    UPDATE billboard_contract_details SET status = 0 WHERE expired_date < CURDATE() AND status = 1;

END
#DELIMETER |
-- End Event detail kontrak expired --

-- Start Event Notifikasi Expired --

CREATE DEFINER=`root`@`localhost` EVENT `contract_expired_notification` 
ON SCHEDULE EVERY 1 DAY 
STARTS '2017-09-26 00:00:00' ON COMPLETION PRESERVE ENABLE 
COMMENT 'buat notifikasi saat kontrak expired' 
DO 
    INSERT INTO notifications (`notification_id`, `notification_type`, `id_user`, `id_sender`, `created_at`, `updated_at`) 
    SELECT id, 'contract_expired', id_customer, NULL, NOW(), NOW() FROM contracts WHERE expired_date = CURDATE() - INTERVAL 1 DAY

-- End Event Notifikasi Expired --

-- Start Event Notifikasi Expired --

CREATE DEFINER=`root`@`localhost` EVENT `contract_will_expired_notification` 
ON SCHEDULE EVERY 1 DAY 
STARTS '2017-10-10 00:00:00' ON COMPLETION PRESERVE ENABLE 
COMMENT 'notifikasi 30 hari sebelum kontrak expired' 
DO 
    INSERT INTO notifications (`notification_id`, `notification_type`, `id_user`, `id_sender`, `created_at`, `updated_at`) 
    SELECT id, 'contract_will_expired', id_customer, NULL, NOW(), NOW() FROM contracts WHERE expired_date = CURDATE() + INTERVAL 1 MONTH

-- End Event Notifikasi Expired --

-- Start Event Expired --

CREATE DEFINER=`root`@`localhost` EVENT `update_event_status` 
ON SCHEDULE EVERY 1 DAY 
STARTS '2017-10-22 00:00:00' ON COMPLETION PRESERVE ENABLE 
COMMENT 'update status event ketika sudah lewat tgl expired' 
DO 
    UPDATE `events` SET status = 0 WHERE expired_date < CURDATE() AND status = 1

-- End Event Expired --