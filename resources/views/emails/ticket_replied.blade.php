@component('mail::message')
# Ticket Dibalas

Halo, {{ $receiver }}.  
{{ $sender }} membalas tiket anda pada billboard dengan kode **{{ $kodeBillboard }}**.  

@component('mail::button', ['url' => $url])
Lihat Tiket
@endcomponent

Thanks,<br>
{{ config('app.name') }}
@endcomponent
