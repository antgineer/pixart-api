@component('mail::message')
# Billboard {{ $status == '1' ? 'Verified' : 'Declined' }}

Halo, {{ $vendor }}.  
Billboard dengan kode **{{ $kodeBillboard }}** telah {{ $status == '1' ? 'Diverifikasi' : 'Ditolak' }}.

@component('mail::button', ['url' => $url])
Lihat Billboard
@endcomponent

Thanks,<br>
{{ config('app.name') }}
@endcomponent
