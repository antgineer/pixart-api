@component('mail::message')
# Ticket Baru Dibuka

Halo, {{ $vendor }}.  
{{ $customer }} membuka tiket pada billboard dengan kode **{{ $kodeBillboard }}**.  

@component('mail::button', ['url' => $url])
Lihat Tiket
@endcomponent

Thanks,<br>
{{ config('app.name') }}
@endcomponent
