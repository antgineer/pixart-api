@component('mail::message')
# Billboard Baru Dibuat

Halo, {{ $vendor }}.  
Billboard dengan kode **{{ $kodeBillboard }}** telah dibuat.

@component('mail::button', ['url' => $url])
Lihat Billboard
@endcomponent

Thanks,<br>
{{ config('app.name') }}
@endcomponent
