@component('mail::message')
# Installasi Billboard Baru

Halo, {{ $customer }}.  
{{ $vendor }} telah membuat installasi billboard baru.  
Dibuat Tanggal : {{ $createdDate }}  
Berakhir Tanggal : {{ $expiredDate }}

@component('mail::button', ['url' => $url])
Lihat Installasi
@endcomponent

Thanks,<br>
{{ config('app.name') }}
@endcomponent
