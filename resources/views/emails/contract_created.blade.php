@component('mail::message')
# Kontrak Billboard Baru

Halo, {{ $customer }}.  
{{ $vendor }} telah membuat kontrak billboard baru.  
Masa aktif : {{ $contractDate }} - {{ $expiredDate }}

@component('mail::button', ['url' => $url])
Lihat Kontrak
@endcomponent

Thanks,<br>
{{ config('app.name') }}
@endcomponent
