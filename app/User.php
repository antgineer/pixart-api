<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Database\Eloquent\SoftDeletes;

class User extends Authenticatable
{
    use Notifiable, SoftDeletes;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'username', 'id_role', 'password', 'parent'
    ];

    protected $dates = ['deleted_at'];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function children(){
        return $this->hasMany('App\User', 'parent');
    }

    public function profile(){
        return $this->hasOne('App\Models\Profile', 'id_user');
    }

    public function billboard(){
        return $this->hasMany('App\Models\Billboard', 'id_user');
    }

    public function contractCustomer(){
        return $this->hasMany('App\Models\Contract', 'id_customer');
    }

    public function contractVendor(){
        return $this->hasMany('App\Models\Contract', 'id_vendor');
    }

    public function billboardRating(){
        return $this->hasMany('App\Models\BillboardRating', 'id_user');
    }

    public function role(){
        return $this->belongsTo('App\Models\Role', 'id_role');
    }

    public function parent(){
        return $this->belongsTo('App\User', 'parent');
    }

    public function ticketCustomer(){
        return $this->hasMany('App\Models\Ticket', 'id_customer');
    }

    public function ticketVendor(){
        return $this->hasMany('App\Models\Ticket', 'id_vendor');
    }

    public function ticketContent(){
        return $this->hasMany('App\Models\TicketContent', 'id_user');
    }

    public function notificationOwner(){
        return $this->hasMany('App\Models\Notification', 'id_user');
    }

    public function notificationSender(){
        return $this->hasMany('App\Models\Notification', 'id_sender');
    }

    public function event(){
        return $this->hasMany('App\Models\Event', 'id_user');
    }
}
