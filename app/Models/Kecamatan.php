<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Kecamatan extends Model
{
    public $timestamps = false;

    protected $fillable = [
        'id_kabupaten', 'nama_kecamatan', 'nama_kecamatan_gmap'
    ];

    public function kelurahan(){
        return $this->hasMany('App\Models\Kelurahan', 'id_kecamatan');
    }

    public function kabupaten(){
        return $this->belongsTo('App\Models\Kabupaten', 'id_kabupaten');
    }
}
