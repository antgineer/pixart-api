<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class TicketContentEvent extends Model
{
    protected $fillable = [
        'content', 'id_ticket', 'id_user'
    ];

    public function ticket(){
        return $this->belongsTo('App\Models\TicketEvent', 'id_ticket');
    }

    public function user(){
        return $this->belongsTo('App\User', 'id_user');
    }
}
