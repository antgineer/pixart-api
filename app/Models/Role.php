<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Role extends Model
{   
    protected $fillable = [
        'role_name', 'status', 'user_created', 'user_modified', 'harga'
    ];

    public function user(){
        return $this->hasMany('App\User', 'id_role');
    }

    public function module(){
        return $this->belongsToMany('App\Models\Module', 'module_role', 'id_role', 'id_module');
    }
}
