<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Contract extends Model
{
    protected $guarded = [
        'created_at', 'updated_at'
    ];

    public function detailContract(){
        return $this->hasMany('App\Models\BillboardContractDetail', 'id_contract');
    }

    public function billboard(){
        return $this->belongsTo('App\Models\Billboard', 'id_billboard');
    }

    public function customer(){
        return $this->belongsTo('App\User', 'id_customer');
    }

    public function vendor(){
        return $this->belongsTo('App\User', 'id_vendor');
    }

    public function notification(){
        return $this->morphMany('App\Models\Notification', 'notification');
    }
}
