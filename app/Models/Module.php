<?php

namespace App\models;

use Illuminate\Database\Eloquent\Model;

class Module extends Model
{
    public $timestamps = false;
    
    protected $fillable = [
        'module_name', 'module_route'
    ];

    public function role(){
        return $this->belongsToMany('App\Models\Role', 'module_role', 'id_module', 'id_role');
    }
}
