<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Billboard extends Model
{
    use SoftDeletes;
    
    protected $guarded = [
        'created_at', 'updated_at', 'deleted_at'
    ];

    protected $dates = ['deleted_at'];

    public function contract(){
        return $this->hasMany('App\Models\Contract', 'id_billboard');
    }

    public function billboardRating(){
        return $this->hasMany('App\Models\BillboardRating', 'id_billboard');
    }

    public function provinsi(){
        return $this->belongsTo('App\Models\Provinsi', 'id_provinsi');
    }

    public function kabupaten(){
        return $this->belongsTo('App\Models\Kabupaten', 'id_kabupaten');
    }

    public function kecamatan(){
        return $this->belongsTo('App\Models\Kecamatan', 'id_kecamatan');
    }

    public function user(){
        return $this->belongsTo('App\User', 'id_user');
    }

    public function photo(){
        return $this->morphMany('App\Models\Photo', 'photo');
    }

    public function video(){
        return $this->morphMany('App\Models\Video', 'video');
    }

    public function notification(){
        return $this->morphMany('App\Models\Notification', 'notification');
    }
}
