<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class BillboardRating extends Model
{
    protected $guarded = [
        'created_at', 'updated_at'
    ];
    
    public function billboard(){
        return $this->belongsTo('App\Models\Billboard', 'id_billboard');
    }

    public function user(){
        return $this->belongsTo('App\Models\User', 'id_user');
    }
}
