<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Photo extends Model
{
    protected $fillable = [
        'photo_name', 'photo_id', 'photo_type'
    ];

    public function photo(){
        return $this->morphTo();
    }
}
