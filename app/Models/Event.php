<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Event extends Model
{
    protected $fillable = [
        'nama_event', 'address', 'jumlah_pengunjung', 'event_date', 'expired_date', 'status', 'id_user', 'note'
    ];

    public function ticket(){
        return $this->hasMany('App\Models\TicketEvent', 'id_event');
    }

    public function user(){
        return $this->belongsTo('App\User', 'id_user');
    }

    public function photo(){
        return $this->morphMany('App\Models\Photo', 'photo');
    }

    public function video(){
        return $this->morphMany('App\Models\Video', 'video');
    }
}
