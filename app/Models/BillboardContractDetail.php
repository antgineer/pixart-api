<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class BillboardContractDetail extends Model
{
    protected $guarded = [
        'created_at', 'updated_at'
    ];

    public function ticket(){
        return $this->hasMany('App\Models\Ticket', 'id_detail_contract');
    }
    
    public function photo(){
        return $this->morphMany('App\Models\Photo', 'photo');
    }

    public function video(){
        return $this->morphMany('App\Models\Video', 'video');
    }

    public function contract(){
        return $this->belongsTo('App\Models\Contract', 'id_contract');
    }

    public function notification(){
        return $this->morphMany('App\Models\Notification', 'notification');
    }

    public function scopeLatestDetailContract($query){
        return $query->whereRaw('created_at = (SELECT MAX(bcd2.created_at)
        FROM billboard_contract_details as bcd2
        WHERE bcd2.id_contract = billboard_contract_details.id_contract)');
    }
}
