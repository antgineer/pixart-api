<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Kabupaten extends Model
{
    public $timestamps = false;

    protected $fillable = [
        'id_provinsi', 'nama_kabupaten', 'nama_kabupaten_gmap'
    ];

    public function kecamatan(){
        return $this->hasMany('App\Models\Kecamatan', 'id_kabupaten');
    }

    public function provinsi(){
        return $this->belongsTo('App\Models\Provinsi', 'id_provinsi');
    }

    public function region(){
        return $this->belongsToMany('App\Models\Region', 'region_kabupaten', 'id_kabupaten', 'id_region');
    }
}
