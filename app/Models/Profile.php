<?php

namespace App\models;

use Illuminate\Database\Eloquent\Model;

class Profile extends Model
{
    protected $fillable = [
        'fullname', 'address', 'phone', 'email', 'id_user'
    ];

    public function user(){
        return $this->belongsTo('App\User', 'id_user');
    }
}
