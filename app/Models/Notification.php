<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Notification extends Model
{
    protected $fillable = [
        'photo_name', 'notification_id', 'notification_type'
    ];

    public function notification(){
        return $this->morphTo();
    }

    public function owner(){
        return $this->belongsTo('App\User', 'id_user');
    }

    public function sender(){
        return $this->belongsTo('App\User', 'id_sender');
    }
}
