<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Region extends Model
{
    public $timestamps = false;

    protected $fillable = [
        'nama_region', 'id_user'
    ];

    public function kabupaten(){
        return $this->belongsToMany('App\Models\Kabupaten', 'region_kabupaten', 'id_region', 'id_kabupaten');
    }
}
