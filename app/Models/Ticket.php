<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Ticket extends Model
{
    protected $fillable = [
        'title', 'status', 'id_customer', 'id_detail_contract', 'closed_date'
    ];

    public function ticketContent(){
        return $this->hasMany('App\Models\TicketContent', 'id_ticket');
    }

    public function customer(){
        return $this->belongsTo('App\User', 'id_customer');
    }

    public function contract(){
        return $this->belongsTo('App\Models\BillboardContractDetail', 'id_detail_contract');
    }
}
