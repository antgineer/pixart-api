<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Video extends Model
{
    protected $fillable = [
        'video_name', 'video_id', 'video_type'
    ];

    public function video(){
        return $this->morphTo();
    }
}
