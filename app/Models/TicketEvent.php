<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class TicketEvent extends Model
{
    protected $fillable = [
        'title', 'status', 'id_customer', 'id_event', 'closed_date'
    ];

    public function ticketContent(){
        return $this->hasMany('App\Models\TicketContentEvent', 'id_ticket');
    }

    public function customer(){
        return $this->belongsTo('App\User', 'id_customer');
    }

    public function event(){
        return $this->belongsTo('App\Models\Event', 'id_event');
    }
}
