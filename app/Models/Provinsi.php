<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Provinsi extends Model
{
    public $timestamps = false;

    protected $fillable = [
        'nama_provinsi', 'nama_provinsi_gmap'
    ];

    public function kabupaten(){
        return $this->hasMany('App\Models\Kabupaten', 'id_provinsi');
    }
}
