<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\TicketContentEvent as TicketContent;
use App\Models\TicketEvent as Ticket;
use Validator;
use App\Http\Controllers\HelperController as Helper;
use App\Http\Controllers\EmailController as Email;

class TicketContentEventController extends Controller
{
    public function store(Request $request, Helper $helper, Email $email){
        $errors = $this->validation($request, $helper);
        
        //jika ada error pd inputan user
        if(!empty($errors)) return response()->json([
            'status'  => 'Error',
            'message' => $errors
        ], 400);

        $ticket = TicketContent::create([
            'content'   => $request->json('content'),
            'id_ticket' => $request->json('id_ticket'),
            'id_user'   => $request->json('id_user')
        ]);

        $id_tiket = $request->json('id_ticket');
        $user = $request->json('id_user');
        $tiket = Ticket::find($id_tiket);

        if($user == $tiket->id_customer){
            $tiket->status = 1;
        }
        else{
            $tiket->status = 2;
        }

        $tiket->save();

        //$email->ticketRepliedEmail($ticket);

        return response()->json([
            'status' => 'Success',
            'data' => $ticket
        ], 200);
    }
    
    public function show($idticket){
    	$ticketcont = TicketContent::where("id_ticket", $idticket)->get();

        //jika tidak ada data ditemukan
        if(empty($ticketcont)) return response()->json([
            'status' => 'Error',
            'message' => 'Ticket data not found'
        ], 403);
        
        return response()->json([
            'status' => 'Success',
            'data' => $ticketcont
        ], 200);
    }
    
    public function validation($request, $helper){
        $validator = Validator::make($request->all(), [
            'content'   => 'bail|required',
            'id_ticket' => 'bail|required|exists:ticket_events,id',
            'id_user'   => 'bail|required|exists:users,id'
        ]);

        if($validator->fails()){
            return $helper->compact($validator->getMessageBag()->toArray());
        }
    }
}
