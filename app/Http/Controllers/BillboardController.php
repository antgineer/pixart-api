<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Models\Billboard;
use App\Models\BillboardRating as Rating;
use App\Models\Photo;
use App\Models\Video;
use JWTAuth;
use App\Http\Controllers\FileController as File;
use App\Http\Controllers\HelperController as Helper;
use App\Http\Controllers\EmailController as Email;
use Illuminate\Validation\Rule;
use Validator;

class BillboardController extends Controller
{
    protected function allBillboard($request, $id = null){
        $page      = $request->page ?: 1;
        $limit     = $request->limit ?: null;
        $orderBy   = $request->orderby ?: 'id';
        $orderDir  = $request->orderdir ?: 'desc';
        $keyword   = $request->keyword ?: null;
        $verify    = $request->verify ?: 'all';
        $startDate = $request->startdate ?: null;
        $endDate   = $request->enddate ?: null;

        $offset = ($page - 1) * $limit;

        $field = ['id', 'address', 'panjang', 'lebar', 'harga'];
        $dir   = ['asc', 'desc'];
        $verification = ['all', 'unverified', 'verified', 'declined'];

        if(!in_array($orderBy, $field) || !in_array($orderDir, $dir) || !in_array($verify, $verification)) return false;

        $billboard = Billboard::select(['billboards.*', 'profiles.fullname as user_fullname', DB::raw('ROUND(AVG(rating), 1) AS rating')])
        ->with(['provinsi', 'kabupaten', 'kecamatan', 'user.profile', 'photo'])
        ->leftJoin('billboard_ratings', 'billboards.id', '=', 'billboard_ratings.id_billboard')
        ->leftJoin('profiles', 'billboards.id_user', '=', 'profiles.id_user')
        ->when(($keyword != null), function($query) use ($keyword){
            return $query->where('address', 'like', "%$keyword%")
                        ->orWhere('panjang', 'like', "%$keyword%")
                        ->orWhere('lebar', 'like', "%$keyword%")
                        ->orWhere('harga', 'like', "%$keyword%");
        })
        ->when(($verify != 'all'), function($query) use ($verify){
            if($verify == 'unverified') return $query->where('verify', 0);
            elseif($verify == 'verified') return $query->where('verify', 1);
            else return $query->where('verify', 2);
        })
        ->when(($id != null), function($query) use ($id){
            return $query->where('billboards.id_user', $id);
        })
        ->when(($startDate != null && $endDate != null), function($query) use ($startDate, $endDate){
            $startDate = date("Y-m-d", strtotime($startDate));
            $endDate = date("Y-m-d", strtotime($endDate.'+ 1 days'));
            return $query->whereBetween('billboards.created_at', [$startDate, $endDate]);
        })
        ->groupBy('billboards.id')
        ->orderBy('billboards.'.$orderBy, $orderDir);

        $data['totalData'] = $billboard->get()->count();
        $data['billboardData'] = $billboard->when(($limit != null), function($query) use($offset, $limit){
            return $query->offset($offset)->limit($limit);
        })->get();

        return $data;
    }

    public function allBillboardDetail($request, $id = null){
        $page      = $request->page ?: 1;
        $limit     = $request->limit ?: null;
        $orderBy   = $request->orderby ?: 'id';
        $orderDir  = $request->orderdir ?: 'desc';
        $keyword   = $request->keyword ?: null;
        $verify    = $request->verify ?: 'all';
        $startDate = $request->startdate ?: null;
        $endDate   = $request->enddate ?: null;
        $status    = $request->status ?: null;

        $offset = ($page - 1) * $limit;

        $field = ['id', 'address', 'panjang', 'lebar', 'harga'];
        $dir   = ['asc', 'desc'];
        $verification = ['all', 'unverified', 'verified', 'declined'];

        if(!in_array($orderBy, $field) || !in_array($orderDir, $dir) || !in_array($verify, $verification)) return false;

        //kayanya querynya ga efisien, will fix later
        $dateDiff = 'DATEDIFF(MAX(DATE(contracts.expired_date)), CURDATE())';

        $subQuery = DB::raw("(SELECT billboards.*, ROUND(AVG(rating)) AS rating, MAX(DATE(contracts.contract_date)) AS contract_date,
                    fullname AS vendor, profiles.address AS vendor_address, profiles.email AS vendor_email,
                    profiles.phone AS vendor_phone, users.photo AS vendor_photo,
                    MAX(DATE(contracts.expired_date)) AS expired_date, if($dateDiff >= 0 || $dateDiff is null, $dateDiff, 'Expired') as days_remaining
                    FROM billboards
                    LEFT JOIN `users` ON billboards.id_user = `users`.id
                    LEFT JOIN `profiles` ON billboards.id_user = `profiles`.id_user
                    LEFT JOIN contracts ON billboards.id = contracts.id_billboard
                    LEFT JOIN billboard_ratings ON billboards.id = billboard_ratings.id_billboard
                    WHERE users.deleted_at IS NULL
                    GROUP BY billboards.id) AS billboards");

        $billboard = Billboard::from($subQuery)
        ->with(['provinsi', 'kabupaten', 'kecamatan', 'photo'])
        ->when(($status == 'unoccupied'), function($query){
            return $query->where(function($query){
                return $query->where('expired_date', '<', DB::raw('CURDATE()'))
                             ->orWhereNull('contract_date');
            });
        })
        ->when(($status == 'occupied'), function($query){
            return $query->where('expired_date', '>=', DB::raw('CURDATE()'));
        })
        ->when(($verify != 'all'), function($query) use ($verify){
            if($verify == 'unverified') return $query->where('verify', 0);
            elseif($verify == 'verified') return $query->where('verify', 1);
            else return $query->where('verify', 2);
        })
        ->when(($id != null), function($query) use ($id){
            return $query->where('billboards.id_user', $id);
        })
        ->when(($startDate != null && $endDate != null), function($query) use ($startDate, $endDate){
            $startDate = date("Y-m-d", strtotime($startDate));
            $endDate = date("Y-m-d", strtotime($endDate.'+ 1 days'));
            return $query->whereBetween('billboards.created_at', [$startDate, $endDate]);
        })
        ->groupBy('billboards.id')
        ->orderBy('billboards.'.$orderBy, $orderDir);

        $data['totalData'] = $billboard->get()->count();
        $data['billboardData'] = $billboard->when(($limit != null), function($query) use($offset, $limit){
            return $query->offset($offset)->limit($limit);
        })->get();

        return $data;
    }

    /**
     * Get all billboard data
     *
     * @param  \Illuminate\Http\Request  $request  (page, limit, orderby = address/panjang/lebar/harga, orderDir = asc/desc, keyword)
     * @return \Illuminate\Http\Response
     */
    public function getAllBillboard(Request $request){
        //jika user adalah admin, tampilkan semua data
        //if($this->userAuth->level == 1){
            $billboard = $this->allBillboard($request);

            if($billboard == false) return response()->json([
                'status' => 'Error',
                'message' => 'Cannot show billboard data'
            ], 404);

            return response()->json([
                'status'     => 'Success',
                'total_data' => $billboard['totalData'],
                'data'       => $billboard['billboardData']
            ], 200);
        //}

        // return response()->json([
        //     'status' => 'Error',
        //     'message' => 'Anda tidak memiliki otorisasi untuk mengakses data billboard'
        // ], 403);
    }

    /**
     * Get all billboard data & detail
     *
     * @param  \Illuminate\Http\Request  $request  (page, limit, orderby = address/panjang/lebar/harga, orderDir = asc/desc, keyword)
     * @return \Illuminate\Http\Response
     */
     public function getAllBillboardDetail(Request $request){
        //jika user adalah admin, tampilkan semua data
        //if($this->userAuth->level == 1){
            $billboard = $this->allBillboardDetail($request);

            if($billboard == false) return response()->json([
                'status' => 'Error',
                'message' => 'Cannot show billboard data'
            ], 404);

            return response()->json([
                'status'     => 'Success',
                'total_data' => $billboard['totalData'],
                'data'       => $billboard['billboardData']
            ], 200);
        //}

        // return response()->json([
        //     'status' => 'Error',
        //     'message' => 'Anda tidak memiliki otorisasi untuk mengakses data billboard'
        // ], 403);
    }

    /**
     * Get all billboard data & detail by vendor
     *
     * @param  \Illuminate\Http\Request  $request  (page, limit, orderby = address/panjang/lebar/harga, orderDir = asc/desc, keyword)
     * @return \Illuminate\Http\Response
     */
     public function getAllBillboardDetailByUser(Request $request, $id){
        $billboard = $this->allBillboardDetail($request, $id);

        if($billboard == false) return response()->json([
            'status' => 'Error',
            'message' => 'Cannot show billboard data'
        ], 404);

        return response()->json([
            'status'     => 'Success',
            'total_data' => $billboard['totalData'],
            'data'       => $billboard['billboardData']
        ], 200);
    }

    /**
     * Search billboard data for Map
     *
     * @param  \Illuminate\Http\Request  $request  (page, limit, orderby = address/panjang/lebar/harga, orderDir = asc/desc, keyword)
     * @return \Illuminate\Http\Response
     */
    public function getAllBillboardMap(Request $request){
        // $id = $this->userAuth->id;
        // $level = $this->userAuth->level;
        $lat = $request->latitude;
        $lng = $request->longitude;
        $kode = $request->kode_billboard;
        $harga = $request->harga;

        if($lat && $lng){
            $distanceQuery = "( 6371 * acos( cos( radians({$lat}) ) *
                            cos( radians( latitude ) ) * cos( radians( longitude ) - radians({$lng}) ) +
                            sin( radians({$lat}) ) * sin( radians( latitude ) ) ) )
                            AS distance,";
        }
        else{
            $distanceQuery = '';
        }

        $dateDiff = 'DATEDIFF(expired_date, CURDATE())';

        $subQuery = DB::raw("(SELECT billboards.*, $distanceQuery ROUND(AVG(rating)) AS rating, contracts.contract_date,
                    fullname as vendor, profiles.address as vendor_address, profiles.email as vendor_email,
                    profiles.phone as vendor_phone, users.photo as vendor_photo,
                    MAX(contracts.expired_date) AS expired_date, if($dateDiff >= 0 || $dateDiff is null, $dateDiff, 'Expired') as days_remaining
                    FROM billboards
                    LEFT JOIN `users` ON billboards.id_user = `users`.id
                    LEFT JOIN `profiles` ON billboards.id_user = `profiles`.id_user
                    LEFT JOIN contracts ON billboards.id = contracts.id_billboard
                    LEFT JOIN billboard_ratings ON billboards.id = billboard_ratings.id_billboard
                    WHERE users.deleted_at IS NULL
                    GROUP BY billboards.id) AS billboards");

        $billboard = Billboard::from($subQuery)
        ->with(['provinsi', 'kabupaten', 'kecamatan', 'photo'])
        ->where(function($query){
            return $query->where('expired_date', '<', DB::raw('(CURDATE() + INTERVAL 7 DAY)'))
                         ->orWhereNull('contract_date');
        })
        ->where('verify', '1')
        ->when(($kode), function($query) use($kode){
            return $query->where('kode_billboard', $kode);
        })
        ->when(($harga && $lat && $lng), function($query) use($harga){
            return $query->where('harga', '<=', $harga);
        })
        ->groupBy('billboards.id')
        ->when(($lat && $lng), function($query){
            return $query->having('distance', '<', 2);
        })
        //->having('distance', '<', 2)
        ->get();

        $total = count($billboard);

        return response()->json([
            'status'     => 'Success',
            'total_data' => $total,
            'data'       => $billboard
        ], 200);
    }

    /**
     * Store new billboard data
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Http\Controllers\FileController  $file
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, Helper $helper, File $file, Email $email)
    {
        $validator = Validator::make($request->all(), [
            'kode_billboard' => 'bail|required|unique:billboards,kode_billboard',
            'id_provinsi'    => 'bail|required',
            'id_kabupaten'   => 'bail|required',
            'id_kecamatan'   => 'bail|required',
            'address'        => 'bail|required',
            'longitude'      => 'bail|required',
            'latitude'       => 'bail|required',
            'panjang'        => 'bail|required',
            'lebar'          => 'bail|required',
            'lighting'       => 'bail|required',
            'side'           => 'bail|required',
            //'arah_pandang'   => 'bail|required',
            'format'         => 'bail|required',
            'bahan'          => 'bail|required',
            'id_user'        => 'bail|required',
            'harga'          => 'bail|required',
            'file.*'         => 'bail|max:2000|file|image|mimes:jpeg,jpg,bmp,png'
        ]);

        if($validator->fails()){
            $errors = $helper->compact($validator->getMessageBag()->toArray());
            return response()->json([
                'status'  => 'Error',
                'message' => $errors
            ], 400);
        }

        $billboard = Billboard::create([
            'kode_billboard' => $request->kode_billboard,
            'id_provinsi'    => $request->id_provinsi,
            'id_kabupaten'   => $request->id_kabupaten,
            'id_kecamatan'   => $request->id_kecamatan,
            'address'        => $request->address,
            'longitude'      => $request->longitude,
            'latitude'       => $request->latitude,
            'panjang'        => $request->panjang,
            'lebar'          => $request->lebar,
            'lighting'       => $request->lighting,
            'side'           => $request->side,
            //'arah_pandang'   => $request->arah_pandang,
            'format'         => $request->format,
            'bahan'          => $request->bahan,
            'id_user'        => $request->id_user,
            'harga'          => str_replace(".","",$request->harga),
            'view_count'     => '0',
            'verify'         => $request->verify,
            'note'           => $request->note
        ]);


        if($request->hasFile('file')){
            //upload storage
            $photo = $file->upload($request->file, 'billboard');

            foreach($photo as $name){
                //insert to photo table
                $image = Photo::create([
                    'photo_name' => $name,
                    'photo_id'   => $billboard->id,
                    'photo_type' => 'billboard'
                ]);
            }
        }

        if($request->hasFile('video')){
            //upload youtube
            $video = $file->uploadVideo($request->video);

            Video::create([
                'video_name' => $video,
                'video_id'   => $billboard->id,
                'video_type' => 'billboard'
            ]);
        }

        $email->billboardCreatedEmail($billboard);

        $billboard = collect($billboard, $billboard->photo, $billboard->video);

        return response()->json([
            'status' => 'Success',
            'data'   => $billboard
        ], 200);
    }

    /**
     * Get single billboard data
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id){
        $billboard = Billboard::select('billboards.*', 'profiles.fullname AS user_fullname',
                    'profiles.address AS user_address', 'profiles.phone AS user_phone')
                    ->leftJoin('profiles', 'billboards.id_user', '=', 'profiles.id_user')
                    ->with(['photo' => function($query){
                        $query->select('id', 'photo_name', 'photo_id');
                    }, 'provinsi', 'kabupaten', 'kecamatan', 'user.profile'])
                    ->find($id);

        //jika tidak ada data ditemukan
        if(empty($billboard)) return response()->json([
            'status' => 'Error',
            'message' => 'Billboard not found'
        ], 403);

        return response()->json([
            'status' => 'Success',
            'data' => $billboard
        ], 200);
    }

    /**
     * Get single billboard data with it's contract
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
     public function showDetail($id){
        $dateDiff = 'DATEDIFF(expired_date, CURDATE())';

        $select = [
            'billboards.*',
            'profiles.fullname as vendor',
            'email as vendor_email',
            'phone as vendor_phone',
            DB::raw('ROUND(AVG(rating), 1) AS rating'),
            DB::raw('DATE(contract_date) AS contract_date'),
            DB::raw('MAX(DATE(expired_date)) AS expired_date'),
            DB::raw("if($dateDiff > 0 || $dateDiff is null, $dateDiff, 'Expired') as days_remaining")
        ];

        $billboard = Billboard::select($select)
                    ->leftJoin('profiles', 'billboards.id_user', '=', 'profiles.id_user')
                    ->leftJoin('contracts', 'billboards.id', '=', 'contracts.id_billboard')
                    ->leftJoin('billboard_ratings', 'billboards.id', '=', 'billboard_ratings.id_billboard')
                    ->with(['photo' => function($query){
                        $query->select('id', 'photo_name', 'photo_id');
                    }, 'provinsi', 'kabupaten', 'kecamatan'])
                    ->groupBy('billboards.id')
                    ->find($id);

        //jika tidak ada data ditemukan
        if(empty($billboard)) return response()->json([
            'status' => 'Error',
            'message' => 'Billboard not found'
        ], 403);

        return response()->json([
            'status' => 'Success',
            'data' => $billboard
        ], 200);
    }

    /**
     * Update billboard data.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Http\Controllers\FileController  $file
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, File $file, Helper $helper, $id){
        $billboard = Billboard::find($id);

        //jika tidak ada data ditemukan
        if(empty($billboard)) return response()->json([
            'status' => 'Error',
            'message' => 'Failed update billboard'
        ], 403);

        $validator = Validator::make($request->all(), [
            'kode_billboard' => ['bail', 'required', Rule::unique('billboards')->ignore($id)],
            'id_provinsi'    => 'bail|required',
            'id_kabupaten'   => 'bail|required',
            'id_kecamatan'   => 'bail|required',
            'address'        => 'bail|required',
            'longitude'      => 'bail|required',
            'latitude'       => 'bail|required',
            'panjang'        => 'bail|required',
            'lebar'          => 'bail|required',
            'lighting'       => 'bail|required',
            'side'           => 'bail|required',
            //'arah_pandang'   => 'bail|required',
            'format'         => 'bail|required',
            'bahan'          => 'bail|required',
            'harga'          => 'bail|required',
        ]);

        if($validator->fails()){
            $errors = $helper->compact($validator->getMessageBag()->toArray());
            return response()->json([
                'status'  => 'Error',
                'message' => $errors
            ], 400);
        }

        //jika user yg meng-update data adalah admin,
        //atau user pemilik billboard, izinkan update
        //if($this->userAuth->level == 1 || $this->userAuth->id == $billboard->id_user){
            $billboard->kode_billboard = $request->kode_billboard;
            $billboard->id_provinsi    = $request->id_provinsi;
            $billboard->id_kabupaten   = $request->id_kabupaten;
            $billboard->id_kecamatan   = $request->id_kecamatan;
            $billboard->address        = $request->address;
            $billboard->longitude      = $request->longitude;
            $billboard->latitude       = $request->latitude;
            $billboard->panjang        = $request->panjang;
            $billboard->lebar          = $request->lebar;
            $billboard->lighting       = $request->lighting;
            $billboard->side           = $request->side;
            //$billboard->arah_pandang   = $request->arah_pandang;
            $billboard->format         = $request->format;
            $billboard->bahan          = $request->bahan;
            $billboard->id_user        = $request->id_user;
            $billboard->harga          = str_replace(".","",$request->harga);
            $billboard->note           = $request->note;
            $billboard->save();

            if($request->hasFile('file')){
                //upload storage
                $photo = $file->upload($request->file, 'billboard');

                foreach($photo as $name){
                    //insert to photo table
                    $image = Photo::create([
                        'photo_name' => $name,
                        'photo_id'   => $billboard->id,
                        'photo_type' => 'billboard'
                    ]);
                }
            }

            if($request->hasFile('video')){
                //upload youtube
                $video = $file->uploadVideo($request->video);
    
                Video::create([
                    'video_name' => $video,
                    'video_id'   => $billboard->id,
                    'video_type' => 'billboard'
                ]);
            }

            $billboard = collect($billboard, $billboard->photo, $billboard->video);

            return response()->json([
                'status' => 'Success',
                'data' => $billboard
            ], 200);
        //}

        // return response()->json([
        //     'status' => 'Error',
        //     'messsage' => 'You have no authorization to update billboard'
        // ], 403);
    }

    /**
     * Delete billboard data.
     *
     * @param  int  $id
     * @param  \App\Http\Controllers\FileController  $file
     * @return \Illuminate\Http\Response
     */
    public function destroy($id, File $file){
        $billboard = Billboard::with(['photo' => function($query){
            $query->select('photo_name', 'photo_id');
        }])->find($id);

        //jika billboard tidak ditemukan
        if(empty($billboard)) return response()->json([
            'status' => 'Error',
            'message' => 'Failed to delete biillboard'
        ], 403);

        //jika user adalah admin, izinkan hapus
        //atau user pemilik billboard, izinkan hapus
        //if($this->userAuth->level == 1 || $this->userAuth->id == $billboard->id_user){
            $billboard->delete();

            //jika billboard punya foto
            if(count($billboard->photo) > 0){
                //delete data foto & file
                $billboard->photo()->where('photo_id', $billboard->id)->delete();
                $file->delete($billboard->photo);
            }

            //jika billboard punya video
            if(count($billboard->video) > 0){
                //delete data video
                $billboard->video()->where('video_id', $billboard->id)->delete();
                $file->deleteVideo($billboard->video);
            }

            return response()->json([
                'status' => 'Success',
                'message' => 'Billboard successfully deleted'
            ], 200);
        //}

        // return response()->json([
        //     'status' => 'Error',
        //     'message' => 'You have no authorization to delete this billboard'
        // ], 403);
    }

    /**
     * Get billboard data by vendor
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function getBillboardByUser(Request $request, $id){
        $billboard = $this->allBillboard($request, $id);

        if($billboard == false) return response()->json([
            'status' => 'Error',
            'message' => 'Cannot show billboard data'
        ], 404);

        return response()->json([
            'status'     => 'Success',
            'total_data' => $billboard['totalData'],
            'data'       => $billboard['billboardData']
        ], 200);
    }

    /**
     * Get billboard contract data by role & user
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
     public function getBillboardContractByUser(Request $request, $role, $id){
        $status = $request->status ?: 'all';

        $select = ['billboards.*', 'contracts.id as id_contract', 'contracts.id_vendor', 'contracts.customer_name as other_customer',
        'contracts.id_customer', 'vp.fullname as vendor_fullname', 'cp.fullname as customer_fullname',
        DB::raw('DATEDIFF(expired_date, CURDATE()) as days_remaining'), DB::raw('DATE(contracts.contract_date) AS contract_date'), DB::raw('DATE(contracts.expired_date) AS expired_date')];

        $billboard = Billboard::select($select)
        ->leftJoin('contracts', 'billboards.id', '=', 'contracts.id_billboard')
        ->leftjoin('profiles as vp', 'contracts.id_vendor', '=', 'vp.id_user')
        ->leftJoin('profiles as cp', 'contracts.id_customer', '=', 'cp.id_user')
        ->when(($status == 'occupied'), function($query){
            return $query->where('expired_date', '>=', DB::raw('CURDATE()'));
        })
        ->when(($status == 'willexpired'), function($query){
            return $query->where('expired_date', '>=', DB::raw('CURDATE()'))
                         ->where('expired_date', '<=',DB::raw('(CURDATE() + INTERVAL 7 DAY)'));
        })
        ->when(($role == 'vendor'), function($query) use ($id){
            return $query->where('billboards.id_user', $id);
        }, function($query) use ($id){
            return $query->where('id_customer', $id);
        })
        ->when(($status == 'unoccupied'), function($query){
            return $query->groupBy('billboards.id')
                         ->havingRaw('ifnull(max(expired_date), (CURDATE() - INTERVAL 1 DAY))  < CURDATE()');
        })
        ->get();

        return response()->json([
            'status' => 'Success',
            'data'   => $billboard
        ], 200);
    }

    /**
     * Store new rating data or update when exist
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function rate(Request $request){
        $rating = Rating::updateOrCreate([
            'id_billboard' => $request->id_billboard,
            'id_user'      => $request->id_user
        ], [
            'rating'       => $request->rating,
        ]);

        return response()->json([
            'status' => 'Success',
            'data'   => $rating
        ], 200);
    }

    /**
     * Update view count when billboard clicked
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function updateViewCount($id){
        $billboard = Billboard::find($id);

        //jika billboard tidak ditemukan
        if(empty($billboard)) return response()->json([
            'status' => 'Error',
            'message' => 'Failed to update billboard view count'
        ], 403);

        $count = ++$billboard->view_count;

        $billboard->view_count = $count;
        $billboard->save();

        return response()->json([
            'status' => 'Success',
            'data'   => [
                'id'    => $billboard->id,
                'count' => $count
            ]
        ], 200);
    }

    /**
     * Verify/Unverify/Reject billboard
     *
     * @param  int  $id
     * @param  string  $type
     * @param  string  $note
     * @return \Illuminate\Http\Response
     */
    public function verify(Request $request, Email $email, $id){
        $type = $request->type;
        $note = $request->note;

        //if($this->userAuth->level == 1){
            $billboard = Billboard::find($id);

            //jika billboard tidak ditemukan
            if(empty($billboard)) return response()->json([
                'status' => 'Error',
                'message' => 'Failed to verify billboard'
            ], 403);

            if($type == 'unverify'){
                $billboard->verify = '0';
                $billboard->reject_note = $note;
                $message = 'Billboard successfully unverified';
            }
            elseif($type == 'verify'){
                $billboard->verify = '1';
                $billboard->reject_note = $note;
                $message = 'Billboard successfully verified';
            }
            elseif($type == 'decline'){
                $billboard->verify = '2';
                $billboard->reject_note = $note;
                $message = 'Billboard successfully rejected';
            }
            else{
                return response()->json([
                    'status' => 'Error',
                    'message' => 'Failed to verify billboard'
                ], 403);
            }

            $billboard->save();

            $email->billboardVerifyEmail($billboard);

            return response()->json([
                'status'  => 'Success',
                'message' => $message,
                'data'    => $billboard
            ], 200);
        //}

        // return response()->json([
        //     'status' => 'Error',
        //     'message' => 'You have no authorization to verify billboard'
        // ], 200);
    }
}
