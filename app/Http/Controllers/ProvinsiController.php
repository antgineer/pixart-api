<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\HelperController as Helper;
use App\Models\Provinsi;
use JWTAuth;
use Validator;

class ProvinsiController extends Controller
{
    /**
     * Attribute auth user.
     *
     * @var array
     */
    //private $userAuth;

    /**
     * Constructor.
     *
     * @return void
     */
    // public function __construct(){
    //     $this->userAuth = JWTAuth::parseToken()->authenticate();
    // }

    /**
     * Display all provinsi data.
     *
     * @return \Illuminate\Http\Response
     */
    public function getAllProvinsi(){
        $data = Provinsi::all();

        return response()->json([
            'status' => 'Success',
            'data' => $data
        ], 200);
    }

    /**
     * Store new provinsi data
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, Helper $helper){
        $validator = Validator::make($request->all(), [
            'nama_provinsi'  => 'bail|required'
        ]);

        if($validator->fails()){
            $errors = $helper->compact($validator->getMessageBag()->toArray());
            return response()->json([
                'status'  => 'Error',
                'message' => $errors
            ], 400);
        }

        //jika user adalah admin, izinkan tambah data
        //if($this->userAuth->level == 1){
            $prov = Provinsi::create([
                'nama_provinsi'      => $request->nama_provinsi,
                'nama_provinsi_gmap' => $request->nama_provinsi_gmap
            ]);

            return response()->json([
                'status' => 'Success',
                'data' => $prov
            ], 200);
        //}

        // return response()->json([
        //     'status' => 'Error',
        //     'message' => 'Anda tidak memiliki otorisasi untuk menambah provinsi'
        // ], 403);
    }

    /**
     * Get single provinsi data
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id){
        $prov = Provinsi::find($id);

        //jika tidak ada data ditemukan
        if(empty($prov)) return response()->json([
            'status' => 'Error',
            'message' => 'Data provinsi tidak ditemukan'
        ], 403);
        
        return response()->json([
            'status' => 'Success',
            'data' => $prov
        ], 200);
    }

    /**
     * Update provinsi data.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Helper $helper, $id){
        $validator = Validator::make($request->all(), [
            'nama_provinsi'  => 'bail|required'
        ]);

        if($validator->fails()){
            $errors = $helper->compact($validator->getMessageBag()->toArray());
            return response()->json([
                'status'  => 'Error',
                'message' => $errors
            ], 400);
        }

        //jika user yg meng-update data adalah admin, izinkan update 
        //if($this->userAuth->level == 1){
            $prov = Provinsi::find($id);

            //jika tidak ada data ditemukan
            if(empty($prov)) return response()->json([
                'status' => 'Error',
                'message' => 'Gagal update data provinsi'
            ], 403);

            $prov->nama_provinsi = $request->nama_provinsi;
            $prov->nama_provinsi_gmap = $request->nama_provinsi_gmap;
            $prov->save();

            return response()->json([
                'status' => 'Success',
                'data' => $prov
            ], 200);
        //}

        // return response()->json([
        //     'status' => 'Error',
        //     'messsage' => 'Anda tidak memiliki otorisasi untuk mengubah provinsi'
        // ], 403);
    }

    /**
     * Delete provinsi data.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id){
        //jika user adalah admin, izinkan hapus
        //if($this->userAuth->level == 1){
            $prov = Provinsi::find($id);

            //jika provinsi tidak ditemukan
            if(empty($prov)) return response()->json([
                'status' => 'Error',
                'message' => 'Data provinsi gagal dihapus'
            ], 403);

            $prov->delete();

            return response()->json([
                'status' => 'Success',
                'message' => 'Data provinsi berhasil dihapus'
            ], 200);
        //}

        // return response()->json([
        //     'status' => 'Error',
        //     'message' => 'Anda tidak memiliki otorisasi untuk menghapus provinsi'
        // ], 403);
    }
}
