<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class HelperController extends Controller
{
    /**
     * Compact array from laravel validator messageBag
     *
     * @param  array  $array
     * @return array
     */
    public function compact($array){
        foreach($array as $key => $value){
            $data[$key] = $value[0];
        }
        return $data;
    }
}
