<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Jobs\SendContractCreatedEmail as ContractCreate;
use App\Jobs\SendContractDetailCreatedEmail as ContractDetailCreate;
use App\Jobs\SendBillboardVerifyEmail as BillboardVerify;
use App\Jobs\SendTicketCreatedEmail as TicketCreate;
use App\Jobs\SendTicketRepliedEmail as TicketReply;
use App\Jobs\SendBillboardCreatedEmail as BillboardCreated;

class EmailController extends Controller
{
    public function contractCreatedEmail($contract){
        $data = [
            'id_contract'    => $contract->id,
            'contract_date'  => date("d-m-Y", strtotime($contract->contract_date)),
            'expired_date'   => date("d-m-Y", strtotime($contract->expired_date)),
            'customer'       => $contract->customer->profile->fullname,
            'customer_email' => $contract->customer->profile->email,
            'vendor'         => $contract->vendor->profile->fullname,
            'url'            => env('WEB_URL').'/contract-details/'.$contract->id
        ];

        dispatch(new ContractCreate( $data ));
    }

    public function contractDetailCreatedEmail($condet){
        $data = [
            'id_contract'    => $condet->id_contract,
            'created_date'   => date("d-m-Y", strtotime($condet->created_at)),
            'expired_date'   => date("d-m-Y", strtotime($condet->expired_date)),
            'customer'       => $condet->contract->customer->profile->fullname,
            'customer_email' => $condet->contract->customer->profile->email,
            'vendor'         => $condet->contract->vendor->profile->fullname,
            'url'            => env('WEB_URL').'/contract-details/'.$condet->id_contract
        ];

        dispatch(new ContractDetailCreate( $data ));
    }

    public function billboardVerifyEmail($billboard){
        $data = [
            'id_billboard'   => $billboard->id,
            'status'         => $billboard->verify,
            'kode_billboard' => $billboard->kode_billboard,
            'vendor'         => $billboard->user->profile->fullname,
            'vendor_email'   => $billboard->user->profile->email,
            'url'            => env('WEB_URL').'/vendor-detailbillboard/'.$billboard->id
        ];

        dispatch(new BillboardVerify( $data ));
    }

    public function ticketCreatedEmail($ticket){
        $data = [
            'id_contract'    => $ticket->contract->id_contract,
            'kode_billboard' => $ticket->contract->contract->billboard->kode_billboard,
            'status'         => $ticket->status,
            'customer'       => $ticket->contract->contract->customer->profile->fullname,
            'vendor'         => $ticket->contract->contract->vendor->profile->fullname,
            'vendor_email'   => $ticket->contract->contract->vendor->profile->email,
            'url'            => env('WEB_URL').'/contract-details/'.$ticket->contract->id_contract
        ];

        dispatch(new TicketCreate( $data ));
    }

    public function ticketRepliedEmail($reply){
        $role = $reply->user->role->role_name;

        $data = [
            'id_contract'    => $reply->ticket->contract->id_contract,
            'kode_billboard' => $reply->ticket->contract->contract->billboard->kode_billboard,
            'url'            => env('WEB_URL').'/contract-details/'.$reply->ticket->contract->id_contract
        ];

        if($role == 'vendor'){
            $data['sender'] = $reply->ticket->contract->contract->vendor->profile->fullname;
            $data['receiver'] = $reply->ticket->contract->contract->customer->profile->fullname;
            $data['receiver_email'] = $reply->ticket->contract->contract->customer->profile->email;
        }
        else{
            $data['sender'] = $reply->ticket->contract->contract->customer->profile->fullname;
            $data['receiver'] = $reply->ticket->contract->contract->vendor->profile->fullname;
            $data['receiver_email'] = $reply->ticket->contract->contract->vendor->profile->email;
        }

        dispatch(new TicketReply( $data ));
    }

    public function billboardCreatedEmail($billboard){
        $data = [
            'id_billboard'   => $billboard->id,
            'kode_billboard' => $billboard->kode_billboard,
            'vendor'         => $billboard->user->profile->fullname,
            'vendor_email'   => $billboard->user->profile->email,
            'url'            => env('WEB_URL').'/vendor-detailbillboard/'.$billboard->id
        ];

        dispatch(new BillboardCreated( $data ));
    }

    public function test(){
        return env('WEB_URL');
    }
}
