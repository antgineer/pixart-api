<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\FileController as File;
use App\Http\Controllers\HelperController as Helper;
use Illuminate\Support\Facades\DB;
use App\Models\Event;
use App\Models\Photo;
use App\Models\Video;
use Validator;

class EventController extends Controller
{
    /**
     * Display all event data.
     *
     * @return \Illuminate\Http\Response
     */
    public function getAllEvent(Request $request){
        $page      = $request->page ?: 1;
        $limit     = $request->limit ?: null;
        $orderBy   = $request->orderby ?: 'id';
        $orderDir  = $request->orderdir ?: 'desc';

        $offset = ($page - 1) * $limit;

        $event = Event::with(['user.profile', 'photo', 'video'])
        ->orderBy($orderBy, $orderDir);

        $data['total'] = $event->count();
        $data['data'] = $event->when($limit, function($query) use($offset, $limit){
            $query->offset($offset)->limit($limit);
        })->get();

        return response()->json([
            'status'     => 'Success',
            'total_data' => $data['total'],
            'data'       => $data['data']
        ], 200);
    }

    /**
     * Store new event data
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, Helper $helper, File $file){
        $errors = $this->validation($request, $helper);

        //jika ada error pd inputan user
        if(!empty($errors)) return response()->json([
            'status'  => 'Error',
            'message' => $errors
        ], 400);

        $event = Event::create([
            'id_user'      => $request->id_user,
            'nama_event'   => $request->nama_event,
            'address'      => $request->address,
            'event_date'   => $request->event_date,
            'expired_date' => $request->expired_date,
            'status'       => 1
        ]);

        return response()->json([
            'status' => 'Success',
            'data'   => $event
        ], 200);
    }

    /**
     * Get single event data
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id){
        $event = Event::select([
            'events.*',
            DB::raw('(select id from events AS ev1
            WHERE ev1.id < events.id
            AND ev1.id_user = events.id_user
            ORDER BY id DESC LIMIT 1) AS previous_id'),
            DB::raw('(select id from events AS ev1
            WHERE ev1.id > events.id
            AND ev1.id_user = events.id_user
            ORDER BY id LIMIT 1) AS next_id')
        ])
        ->with(['user.profile', 'photo', 'ticket' => function($query){
            return $query->with(['ticketContent.user.profile'])->where('status', '<>', 0);
        }])
        ->find($id);

        //jika tidak ada data ditemukan
        if(empty($event)) return response()->json([
            'status' => 'Error',
            'message' => 'Data event tidak ditemukan'
        ], 403);

        return response()->json([
            'status' => 'Success',
            'data' => $event
        ], 200);
    }

    /**
     * Update event data.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Helper $helper, File $file, $id){
        // return $request->all();
        $errors = $this->validation($request, $helper);

        //jika ada error pd inputan user
        if(!empty($errors)) return response()->json([
            'status'  => 'Error',
            'message' => $errors
        ], 400);

        $event = Event::find($id);

        //jika tidak ada data ditemukan
        if(empty($event)) return response()->json([
            'status' => 'Error',
            'message' => 'Gagal update data event'
        ], 403);

        $event->nama_event        = $request->nama_event;
        $event->address           = $request->address;
        $event->event_date        = $request->event_date;
        $event->expired_date      = $request->expired_date;
        $event->id_user           = $request->id_user;
        $event->status            = $request->status;
        $event->save();

        return response()->json([
            'status' => 'Success',
            'data'   => $event
        ], 200);
    }

    /**
     * Update event data eo.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function updateEo(Request $request, Helper $helper, File $file, $id){
        $event = Event::find($id);

        //jika tidak ada data ditemukan
        if(empty($event)) return response()->json([
            'status' => 'Error',
            'message' => 'Gagal update data event'
        ], 403);

        $event->jumlah_pengunjung = $request->jumlah_pengunjung;
        $event->note              = $request->note;
        $event->save();

        if($request->hasFile('file')){
            //upload storage
            $photo = $file->upload($request->file, 'event');

            foreach($photo as $name){
                //insert to photo table
                $image = Photo::create([
                    'photo_name' => $name,
                    'photo_id'   => $event->id,
                    'photo_type' => 'event'
                ]);
            }
        }

        if($request->hasFile('video')){
            //upload youtube
            $video = $file->uploadVideo($request->video);

            Video::create([
                'video_name' => $video,
                'video_id'   => $event->id,
                'video_type' => 'event'
            ]);
        }

        $event = collect($event, $event->photo, $event->video);

        return response()->json([
            'status' => 'Success',
            'data'   => $event
        ], 200);
    }

    /**
     * Delete region data.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(File $file, $id){
        $event = Event::find($id);

        //jika event tidak ditemukan
        if(empty($event)) return response()->json([
            'status' => 'Error',
            'message' => 'Data event gagal dihapus'
        ], 403);

        $event->delete();

        //jika event punya foto
        if(count($event->photo) > 0){
            //delete data foto & file
            $event->photo()->where('photo_id', $event->id)->delete();
            $file->delete($event->photo);
        }

        //jika event punya video
        if(count($event->video) > 0){
            //delete data video
            $event->video()->where('video_id', $event->id)->delete();
            $file->deleteVideo($event->video);
        }

        return response()->json([
            'status' => 'Success',
            'message' => 'Data event berhasil dihapus'
        ], 200);
    }

    /**
     * Validate user input.
     *
     * @param  object  $request
     * @param  object  $helper
     * @return array
     */
    public function validation($request, $helper){
        $validator = Validator::make($request->all(), [
            'nama_event'        => 'bail|required',
            'address'           => 'bail|required',
            //'jumlah_pengunjung' => 'bail|required|numeric',
            'event_date'        => 'bail|required|date',
            'expired_date'      => 'bail|required|date',
            'id_user'           => 'bail|required|exists:users,id',
        ]);

        if($validator->fails()){
            return $helper->compact($validator->getMessageBag()->toArray());
        }
    }

    /**
     * Get single event data
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function getEventByUser(Request $request, $id){
        $page      = $request->page ?: 1;
        $limit     = $request->limit ?: null;
        $orderBy   = $request->orderby ?: 'id';
        $orderDir  = $request->orderdir ?: 'desc';

        $offset = ($page - 1) * $limit;

        $event = Event::select([
            'events.*',
            DB::raw('(select id from events AS ev1
            WHERE ev1.id < events.id
            AND ev1.id_user = events.id_user
            ORDER BY id DESC LIMIT 1) AS previous_id'),
            DB::raw('(select id from events AS ev1
            WHERE ev1.id > events.id
            AND ev1.id_user = events.id_user
            ORDER BY id LIMIT 1) AS next_id')
        ])
        ->with(['user.profile', 'photo'])
        ->where('id_user', $id)
        ->orderBy($orderBy, $orderDir);

        //jika tidak ada data ditemukan

        if(empty($event)) return response()->json([
            'status' => 'Error',
            'message' => 'Data event tidak ditemukan'
        ], 403);
        $data['total'] = $event->count();
        $data['data'] = $event->when($limit, function($query) use($offset, $limit){
            $query->offset($offset)->limit($limit);
        })->get();

        return response()->json([
            'status'     => 'Success',
            'total_data' => $data['total'],
            'data'       => $data['data']
        ], 200);
    }

     /**
     * Close event
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function closeEvent($id){
        $event = Event::find($id);

        //jika tidak ada data ditemukan
        if(empty($event)) return response()->json([
            'status' => 'Error',
            'message' => 'Data event tidak ditemukan'
        ], 403);

        $event->status = '0';
        $event->save();

        return response()->json([
            'status' => 'Success',
            'data' => $event
        ], 200);
    }
}
