<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Module;
use App\Http\Controllers\HelperController as Helper;
use Validator;

class ModuleController extends Controller
{
    /**
     * Display all module data.
     *
     * @return \Illuminate\Http\Response
     */
     public function getAllModule(){
        $data = Module::all();

        return response()->json([
            'status' => 'Success',
            'data' => $data
        ], 200);
    }

    /**
     * Store new module data
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
     public function store(Request $request, Helper $helper){
        $validator = Validator::make($request->all(), [
            'module_name'  => 'bail|required',
            'module_route' => 'bail|required'
        ]);

        if($validator->fails()){
            $errors = $helper->compact($validator->getMessageBag()->toArray());
            return response()->json([
                'status'  => 'Error',
                'message' => $errors
            ], 400);
        }

        //jika user adalah admin, izinkan tambah data
        //if($this->userAuth->level == 1){
            $module = Module::create([
                'module_name'  => $request->module_name,
                'module_route' => $request->module_route
            ]);

            return response()->json([
                'status' => 'Success',
                'data' => $module
            ], 200);
        //}

        // return response()->json([
        //     'status' => 'Error',
        //     'message' => 'Anda tidak memiliki otorisasi untuk menambah module'
        // ], 403);
    }

    /**
     * Get single module data
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
     public function show($id){
        $module = Module::find($id);

        //jika tidak ada data ditemukan
        if(empty($module)) return response()->json([
            'status' => 'Error',
            'message' => 'Data module tidak ditemukan'
        ], 403);
        
        return response()->json([
            'status' => 'Success',
            'data' => $module
        ], 200);
    }

    /**
     * Update module data.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
     public function update(Request $request, Helper $helper, $id){
        $validator = Validator::make($request->all(), [
            'module_name'  => 'bail|required',
            'module_route' => 'bail|required'
        ]);

        if($validator->fails()){
            $errors = $helper->compact($validator->getMessageBag()->toArray());
            return response()->json([
                'status'  => 'Error',
                'message' => $errors
            ], 400);
        }

        //jika user yg meng-update data adalah admin, izinkan update 
        //if($this->userAuth->level == 1){
            $module = Module::find($id);

            //jika tidak ada data ditemukan
            if(empty($module)) return response()->json([
                'status'  => 'Error',
                'message' => 'Gagal update data module'
            ], 403);

            $module->module_name  = $request->module_name;
            $module->module_route = $request->module_route;
            $module->save();

            return response()->json([
                'status' => 'Success',
                'data' => $module
            ], 200);
        //}

        // return response()->json([
        //     'status' => 'Error',
        //     'messsage' => 'Anda tidak memiliki otorisasi untuk mengubah module'
        // ], 403);
    }

    /**
     * Delete module data.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
     public function destroy($id){
        //jika user adalah admin, izinkan hapus
        //if($this->userAuth->level == 1){
            $module = Module::find($id);

            //jika module tidak ditemukan
            if(empty($module)) return response()->json([
                'status' => 'Error',
                'message' => 'Data module gagal dihapus'
            ], 403);

            $module->delete();

            return response()->json([
                'status' => 'Success',
                'message' => 'Data module berhasil dihapus'
            ], 200);
        //}

        // return response()->json([
        //     'status' => 'Error',
        //     'message' => 'Anda tidak memiliki otorisasi untuk menghapus module'
        // ], 403);
    }
}
