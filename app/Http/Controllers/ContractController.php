<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Billboard;
use Illuminate\Support\Facades\DB;
use App\Models\Contract;
use App\User;
use Validator;
use JWTAuth;
use App\Http\Controllers\FileController as File;
use App\Http\Controllers\HelperController as Helper;
use App\Http\Controllers\EmailController as Email;

class ContractController extends Controller
{
    /**
     * Attribute auth user.
     *
     * @var array
     */
    //private $userAuth;

    /**
     * Constructor.
     *
     * @return void
     */
    public function __construct(){
        //$this->userAuth = JWTAuth::parseToken()->authenticate();
    }

    /**
     * Display all contract data.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(){
        $contract = Contract::all();

        return response()->json([
            'status' => 'Success',
            'data'   => $contract
        ], 200);
    }

    /**
     * Store new contract data
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  App\Http\Controllers\HelperController  $helper
     * @param  \App\Http\Controllers\FileController  $file
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, Helper $helper, File $file, Email $email){
        $errors = $this->validation($request, $helper);
        
        //jika ada error pd inputan user
        if(!empty($errors)) return response()->json([
            'status'  => 'Error',
            'message' => $errors
        ], 400);

        //ambil id user pemilik billboard
        //$billboard = Billboard::select('id_user')->find($request->id_billboard);
        
        //jika user adalah admin
        //atau pemilik billboard, izinkan buat contract
        //if($this->userAuth->level == 1 || $this->userAuth->id == $billboard->id_user){
            $contract = Contract::create([
                'expired_date'  => $request->expired_date,
                'contract_date' => $request->contract_date,
                'id_billboard'  => $request->id_billboard,
                'id_customer'   => $request->id_customer,
                'id_vendor'     => $request->id_vendor,
                'customer_name' => $request->customer_name
            ]);

            if($request->id_customer){
                $email->contractCreatedEmail($contract);
            }

            return response()->json([
                'status' => 'Success',
                'data'   => $contract
            ], 200);
        //}

        // return response()->json([
        //     'status'  => 'Error',
        //     'message' => 'You have no authorization to add new contract'
        // ], 403);
    }

    /**
     * Get single contract data
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id){
        $contract = Contract::select('contracts.id', 'contracts.id_billboard', 'contracts.id_customer', 'contracts.customer_name', 
        'contracts.id_vendor', DB::raw('DATE(contracts.contract_date) AS contract_date'), 
        DB::raw('DATE(contracts.expired_date) AS expired_date'))
        ->with([
            'billboard' => function($query){
                return $query->with(['provinsi', 'kabupaten', 'kecamatan', 'photo'])->withTrashed();
            },
            'vendor' => function($query){
                return $query->select('id', 'username', 'photo')->with(['profile' => function($query){
                    return $query->select('id', 'fullname', 'email', 'address', 'phone', 'id_user');
                }]);
            }, 
            'customer' => function($query){
                return $query->select('id', 'username', 'photo')->with(['profile' => function($query){
                    return $query->select('id', 'fullname', 'email', 'address', 'phone', 'id_user');
                }]);
            },
            'detailContract' => function($query){
                return $query->select([
                    'billboard_contract_details.id', 
                    'billboard_contract_details.tema_iklan', 
                    'billboard_contract_details.brand', 
                    'billboard_contract_details.status', 
                    DB::raw('DATE(billboard_contract_details.created_at) AS created_date'), 
                    DB::raw('DATE(billboard_contract_details.expired_date) AS expired_date'), 
                    'billboard_contract_details.id_contract', 
                    'billboard_contract_details.id_user',
                    DB::raw('(select id from billboard_contract_details AS bcd1 
                    WHERE bcd1.id < billboard_contract_details.id
                    AND bcd1.id_contract = billboard_contract_details.id_contract
                    ORDER BY id DESC LIMIT 1) AS previous_id')
                ])
                ->with([
                    'photo' => function($query){
                        $query->select('photo_name', 'photo_id');
                    },
                    'ticket'=>function($query){
                        $query->select('title', 'status','id_detail_contract','id')->latest()->where('status','<>',0)
                        ->with(['ticketContent'=>function($query){
                            $query->select('id_ticket','content','id_user')
                            ->with(['user'=>function($query){
                                $query->select('id','username','photo')->with(['profile' => function($query){
                                    return $query->select('fullname','id_user');
                                }]);
                            }]);
                        }]);
                    }
                ])->latest();
            }
        ])
        ->find($id);

        //jika tidak ada data ditemukan
        if(empty($contract)) return response()->json([
            'status'  => 'Error',
            'message' => 'Contract not found'
        ], 403);
        
        return response()->json([
            'status' => 'Success',
            'data'   => $contract
        ], 200);
    }

    /**
     * Update contract data.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  App\Http\Controllers\HelperController  $helper
     * @param  \App\Http\Controllers\FileController  $file
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Helper $helper, File $file, $id){
        $errors = $this->validation($request, $helper);
        
        //jika ada error pd inputan user
        if(!empty($errors)) return response()->json([
            'status'  => 'Error',
            'message' => $errors
        ], 400);

        //ambil id user pemilik billboard
        //$billboard = Billboard::select('id_user')->find($request->id_billboard);

        //jika user yg meng-update data adalah admin,
        //atau pemilik contract(vendor) izinkan update
        //if($this->userAuth->level == 1 || $this->userAuth->id == $billboard->id_user){
            $contract = Contract::find($id);

            //jika tidak ada data ditemukan
            if(empty($contract)) return response()->json([
                'status'  => 'Error',
                'message' => 'Failed to update contract'
            ], 403);

            $contract->expired_date  = $request->expired_date;
            $contract->contract_date = $request->contract_date;
            $contract->id_billboard  = $request->id_billboard;
            $contract->id_customer   = $request->id_customer;
            $contract->id_vendor     = $request->id_vendor;
            $contract->customer_name = $request->customer_name;
            $contract->save();

            return response()->json([
                'status' => 'Success',
                'data'   => $contract
            ], 200);
        //}

        // return response()->json([
        //     'status'   => 'Error',
        //     'messsage' => 'You have no authorization to update this contract'
        // ], 403);
    }

    /**
     * Delete contract data.
     *
     * @param  int  $id
     * @param  \App\Http\Controllers\FileController  $file
     * @return \Illuminate\Http\Response
     */
    public function destroy($id, File $file){
        $contract = Contract::with(['photo' => function($query){
            $query->select('photo_name', 'photo_id');
        }])->find($id);

        //jika contract tidak ditemukan
        if(empty($contract)) return response()->json([
            'status'  => 'Error',
            'message' => 'Failed to delete contract'
        ], 403);

        //ambil id user pemilik billboard
        $billboard = Billboard::select('id_user')->find($contract->id_billboard);

        //jika user adalah admin
        //atau pemilik contract(vendor), izinkan hapus
        //if($this->userAuth->level == 1 || $this->userAuth->id == $billboard->id_user){
            $contract->delete();

            //jika contract punya foto
            if(count($contract->photo) > 0){
                //delete data foto & file
                $contract->photo()->where('photo_id', $contract->id)->delete();
                $file->delete($contract->photo);
            }

            return response()->json([
                'status'  => 'Success',
                'message' => 'Contract successfully deleted'
            ], 200);
        //}

        // return response()->json([
        //     'status'  => 'Error',
        //     'message' => 'You have no authorization to delete this contract'
        // ], 403);
    }

    /**
     * Validate user input.
     *
     * @param  object  $request
     * @param  object  $helper
     * @return array
     */
    public function validation($request, $helper){
        $validator = Validator::make($request->all(), [
            'expired_date'  => 'bail|required|date',
            'contract_date' => 'bail|required|date',
            'id_billboard'  => 'bail|required|exists:billboards,id',
            //'id_customer'   => 'bail|required',
            'id_vendor'     => 'bail|required|exists:users,id',
        ]);

        if($validator->fails()){
            return $helper->compact($validator->getMessageBag()->toArray());
        }
    }

    /**
     * Get contract by user (vendor|customer).
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @param  string  $role
     * @return \Illuminate\Http\Response
     */
    public function getContractByUser(Request $request, $role, $id){
        $page      = $request->page ?: 1;
        $limit     = $request->limit ?: null;
        $orderBy   = $request->orderby ?: 'id';
        $orderDir  = $request->orderdir ?: 'desc';

        //filter by contract status (all, occupied, willexpired, newupdated, contractexpired, detailexpired)
        $status = $request->status ?: 'all';

        //filter by other parameter
        $vendor         = $request->vendor;
        $kode_billboard = $request->kode_billboard;
        $provinsi       = $request->provinsi;
        $kabupaten      = $request->kabupaten;
        $kecamatan      = $request->kecamatan;
        $region         = $request->region;
        $address        = $request->address;
        $brand          = $request->brand;
        $tema           = $request->tema;
        $panjang        = $request->panjang;
        $lebar          = $request->lebar;

        $startDate      = $request->start_date;
        $endDate        = $request->end_date;

        $latitude       = $request->latitude ?: null;
        $longitude      = $request->longitude ?: null;
        
        $contract = Contract::select([
            'contracts.id', 'contracts.id_billboard', 'contracts.id_customer', 
            'contracts.customer_name', 'contracts.id_vendor', 
            DB::raw('DATE(contracts.contract_date) AS contract_date'), 
            DB::raw('DATE(contracts.expired_date) AS expired_date'),
            DB::raw("(CASE 
                WHEN contracts.expired_date < CURDATE() THEN 'Expired Contract'
                WHEN DATE(bcd.created_at) = CURDATE() THEN 'Now Updated'
                WHEN contracts.expired_date >= CURDATE() && DATE(bcd.expired_date) < CURDATE() THEN 'Expired Installment'
                WHEN contracts.expired_date >= CURDATE() && contracts.expired_date <= CURDATE() + INTERVAL 3 MONTH THEN 'Will Expired'
                ELSE 'Active'
            END) AS contract_status")
        ])
        ->with([
            'billboard' => function($query) use ($id){
                return $query->with(['provinsi', 'kabupaten.region' => function($query) use ($id){
                    return $query->where('id_user', $id);
                }, 'kecamatan', 'photo'])->withTrashed();
            }, 
            'vendor' => function($query){
                return $query->select('id', 'username', 'photo')->with(['profile' => function($query){
                    return $query->select('id', 'fullname', 'email', 'address', 'phone', 'id_user');
                }]);
            }, 
            'customer' => function($query){
                return $query->select('id', 'username', 'photo')->with(['profile' => function($query){
                    return $query->select('id', 'fullname', 'email', 'address', 'phone', 'id_user');
                }]);
            }, 
            'detailContract' => function($query){
                return $query->with(['photo' => function($query){
                    $query->select('photo_name', 'photo_id');
                }])
                ->latestDetailContract();
            }
        ])
        ->leftJoin('billboard_contract_details AS bcd', 'bcd.id', '=', DB::raw("(SELECT bcd2.id FROM billboard_contract_details AS bcd2 
        WHERE contracts.id = bcd2.id_contract ORDER BY bcd2.id DESC LIMIT 1)"))

        //filter by role
        ->when(($role == 'vendor'), function($query) use ($id){
            return $query->where('id_vendor', $id);
        }, function($query) use ($id){
            return $query->where('id_customer', $id);
        })

        //filter by other parameter
        ->when($vendor, function($query) use ($vendor){
            return $query->whereHas('vendor.profile', function($query) use ($vendor){
                return $query->where('fullname', 'like', "%$vendor%");
            });
        })
        ->when($kode_billboard, function($query) use ($kode_billboard){
            return $query->whereHas('billboard', function($query) use ($kode_billboard){
                return $query->where('kode_billboard', 'like', "%$kode_billboard%");
            });
        })
        ->when($startDate, function($query) use ($startDate){
            return $query->where(function($query) use ($startDate){
                return $query->where('contract_date', '>=', $startDate);;
            });
        })
        ->when($endDate, function($query) use ($endDate){
            return $query->where(function($query) use ($endDate){
                return $query->where('contracts.expired_date', '<=', $endDate);
            });
        })
        ->when($provinsi, function($query) use ($provinsi){
            return $query->whereHas('billboard', function($query) use ($provinsi){
                return $query->where('id_provinsi', $provinsi)->withTrashed();
            });
        })
        ->when($kabupaten, function($query) use ($kabupaten){
            return $query->whereHas('billboard', function($query) use ($kabupaten){
                return $query->where('id_kabupaten', $kabupaten)->withTrashed();
            });
        })
        ->when($kecamatan, function($query) use ($kecamatan){
            return $query->whereHas('billboard', function($query) use ($kecamatan){
                return $query->where('id_kecamatan', $kecamatan)->withTrashed();
            });
        })
        ->when($region, function($query) use ($region, $id){
            return $query->whereHas('billboard', function($query) use ($region, $id){
                return $query->whereHas('kabupaten', function($query) use ($region, $id){
                    return $query->whereHas('region', function($query) use ($region, $id){
                        return $query->where('id_region', $region)->where('id_user', $id);
                    });
                });
            });
        })
        ->when($address, function($query) use ($address){
            return $query->whereHas('billboard', function($query) use ($address){
                return $query->where('address', 'like', "%$address%")->withTrashed();
            });
        })
        ->when($panjang, function($query) use ($panjang){
            return $query->whereHas('billboard', function($query) use ($panjang){
                return $query->where('panjang', $panjang)->withTrashed();
            });
        })
        ->when($lebar, function($query) use ($lebar){
            return $query->whereHas('billboard', function($query) use ($lebar){
                return $query->where('lebar', $lebar)->withTrashed();
            });
        })
        ->when($brand, function($query) use ($brand){
            return $query->whereHas('detailContract', function($query) use ($brand){
                return $query->where('brand', 'like', "%$brand%");
            });
        })
        ->when($tema, function($query) use ($tema){
            return $query->whereHas('detailContract', function($query) use ($tema){
                return $query->where('tema_iklan', 'like', "%$tema%");
            });
        })
        ->when($latitude && $longitude, function($query) use ($latitude, $longitude){
            return $query->whereHas('billboard', function($query) use ($latitude, $longitude){
                return $query->select(DB::raw("( 6371 * acos( cos( radians({$latitude}) ) *
                cos( radians( latitude ) ) * cos( radians( longitude ) - radians({$longitude}) ) +
                sin( radians({$latitude}) ) * sin( radians( latitude ) ) ) )
                AS distance"))
                ->having('distance', '<=', 50) //in kilometer
                ->withTrashed();
            });
        })

        //filter by status contract
        ->when(($status == 'occupied'), function($query){
            return $query->where('contracts.expired_date', '>=', DB::raw('CURDATE()'));
        })
        ->when(($status == 'contractexpired'), function($query){
            return $query->where('contracts.expired_date', '<', DB::raw('CURDATE()'));
        })
        ->when(($status == 'detailexpired'), function($query){
            return $query->where('contracts.expired_date', '>=', DB::raw('CURDATE()'))
            ->whereHas('detailContract', function($query){
                return $query->select('id', DB::raw('MAX(expired_date) AS exp'), 'id_contract')
                ->groupBy('id_contract')
                ->having('exp', '<', DB::raw('CURDATE()'));
            });
        })
        ->when(($status == 'willexpired'), function($query){
            return $query->where(function($query){
                return $query->where('contracts.expired_date', '>=', DB::raw('CURDATE()'))
                             ->where('contracts.expired_date', '<=', DB::raw('(CURDATE() + INTERVAL 3 MONTH)'));
            });
        })
        ->when(($status == 'newupdated'), function($query){
            return $query->whereHas('detailContract', function($query){
                return $query->where(DB::raw('DATE(created_at)'), DB::raw('CURDATE()'));
            });
        })
        ->orderBy($orderBy, $orderDir);

        $totalData = $contract->get()->count();
        $data =  $contract->when(($limit != null), function($query) use($page, $limit){
            $offset = ($page - 1) * $limit;
            return $query->offset($offset)->limit($limit);
        })->get();

        return response()->json([
            'status'     => 'Success',
            'total_data' => $totalData,
            'data'       => $data
        ], 200);
    }

    /**
     * Get contract by billboard.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function getContractByBillboard($id){
        $contract = Contract::select('contracts.*', 'vp.fullname as vendor_fullname', 'cp.fullname as customer_fullname')
        ->with(['billboard.provinsi', 'billboard.kabupaten', 'billboard.kecamatan', 'detailContract' => function($query){
            return $query->with('photo');
        }])
        ->leftJoin('profiles as vp', 'contracts.id_vendor', '=', 'vp.id_user')
        ->leftJoin('profiles as cp', 'contracts.id_customer', '=', 'cp.id_user')
        ->find($id);

        //jika tidak ada data ditemukan
        if(empty($contract)) return response()->json([
            'status'  => 'Error',
            'message' => 'Contract not found'
        ], 403);
        
        return response()->json([
            'status' => 'Success',
            'data'   => $contract
        ], 200);
    }
}
