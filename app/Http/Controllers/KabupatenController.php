<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\HelperController as Helper;
use App\Models\Provinsi;
use App\Models\Kabupaten;
use JWTAuth;
use Validator;

class KabupatenController extends Controller
{
    /**
     * Attribute auth user.
     *
     * @var array
     */
    //private $userAuth;

    /**
     * Constructor.
     *
     * @return void
     */
    // public function __construct(){
    //     $this->userAuth = JWTAuth::parseToken()->authenticate();
    // }

    /**
     * Get all kabupaten data
     *
     * @param  int  $page
     * @param  int  $limit
     * @param  string  $orderBy  (name)
     * @param  string  $orderDir  (asc, desc)
     * @param  string  $keyword
     * @return \Illuminate\Http\Response
     */
    public function getAllKabupaten($page = 1, $limit = 10, $orderBy = 'id', $orderDir = 'asc', $keyword = null){
        $offset = ($page - 1) * $limit;
        $orderDir = 'desc'; //sementara

        $kabupaten = Kabupaten::when(($keyword != null), function($query) use ($keyword){
            return $query->where('nama_kabupaten', 'like', "%$keyword%");
        })->orderBy($orderBy, $orderDir)
          ->offset($offset)->limit($limit)
          ->get();

        $totalData = Kabupaten::count();
          
        return response()->json([
            'status' => 'Success',
            'total_data' => $totalData,
            'data' => $kabupaten
        ], 200);
    }

    /**
     * Get all kabupaten by provinsi.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function getKabupatenByProv($id){
        $data = Kabupaten::where('id_provinsi', $id)->get();

        //jika tidak ada data ditemukan
        if(count($data) == 0) return response()->json([
            'status' => 'Error',
            'message' => 'Data kabupaten tidak ditemukan'
        ], 400);

        return response()->json([
            'status' => 'Success',
            'data' => $data
        ], 200);
    }

    /**
     * Get all kabupaten, for autocomplete
     *
     * @param  string  $name
     * @param  integer  $limit
     * @return \Illuminate\Http\Response
     */
    // public function getAllKabupaten($name = '', $limit = 10){
    //     $data = Kabupaten::select('id', 'nama_kabupaten')->where('nama_kabupaten', 'like', "%$name%")->limit($limit)->get();

    //     return response()->json([
    //         'status' => 'Success',
    //         'data' => $data
    //     ], 200);
    // }

    /**
     * Store new kabupaten data
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, Helper $helper){
        $validator = Validator::make($request->all(), [
            'id_provinsi'   => 'bail|required',
            'nama_kabupaten' => 'bail|required'
        ]);

        if($validator->fails()){
            $errors = $helper->compact($validator->getMessageBag()->toArray());
            return response()->json([
                'status'  => 'Error',
                'message' => $errors
            ], 400);
        }

        //jika user adalah admin, izinkan tambah data
        //if($this->userAuth->level == 1){
            $prov = Provinsi::find($request->id_provinsi);

            if(empty($prov)) return response()->json([
                'status' => 'Error',
                'message' => 'Gagal menyimpan data kabupaten'
            ], 403);

            $kab = Kabupaten::create([
                'id_provinsi'         => $request->id_provinsi,
                'nama_kabupaten'      => $request->nama_kabupaten,
                'nama_kabupaten_gmap' => $request->nama_kabupaten_gmap
            ]);

            return response()->json([
                'status' => 'Success',
                'data' => $kab
            ], 200);
        //}

        // return response()->json([
        //     'status' => 'Error',
        //     'message' => 'Anda tidak memiliki otorisasi untuk menambah kabupaten'
        // ], 403);
    }

    /**
     * Get single kabupaten data
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id){
        $kab = Kabupaten::find($id);

        //jika tidak ada data ditemukan
        if(empty($kab)) return response()->json([
            'status' => 'Error',
            'message' => 'Data kabupaten tidak ditemukan'
        ], 403);
        
        return response()->json([
            'status' => 'Success',
            'data' => $kab
        ], 200);
    }

    /**
     * Update kabupaten data.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Helper $helper, $id){
        $validator = Validator::make($request->all(), [
            'id_provinsi'   => 'bail|required',
            'nama_kabupaten' => 'bail|required'
        ]);

        if($validator->fails()){
            $errors = $helper->compact($validator->getMessageBag()->toArray());
            return response()->json([
                'status'  => 'Error',
                'message' => $errors
            ], 400);
        }

        //jika user yg meng-update data adalah admin, izinkan update 
        //if($this->userAuth->level == 1){
            $kab = Kabupaten::find($id);

            //jika tidak ada data ditemukan
            if(empty($kab)) return response()->json([
                'status' => 'Error',
                'message' => 'Gagal update data kabupaten'
            ], 403);

            $kab->id_provinsi = $request->id_provinsi;
            $kab->nama_kabupaten = $request->nama_kabupaten;
            $kab->nama_kabupaten_gmap = $request->nama_kabupaten_gmap;
            $kab->save();

            return response()->json([
                'status' => 'Success',
                'data' => $kab
            ], 200);
        //}

        // return response()->json([
        //     'status' => 'Error',
        //     'messsage' => 'Anda tidak memiliki otorisasi untuk mengubah kabupaten'
        // ], 403);
    }

    /**
     * Delete kabupaten data.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id){
        //jika user adalah admin, izinkan hapus
        //if($this->userAuth->level == 1){
            $kab = Kabupaten::find($id);

            //jika kabupaten tidak ditemukan
            if(empty($kab)) return response()->json([
                'status' => 'Error',
                'message' => 'Data kabupaten gagal dihapus'
            ], 403);

            $kab->delete();

            return response()->json([
                'status' => 'Success',
                'message' => 'Data kabupaten berhasil dihapus'
            ], 200);
        //}

        // return response()->json([
        //     'status' => 'Error',
        //     'message' => 'Anda tidak memiliki otorisasi untuk menghapus kabupaten'
        // ], 403);
    }
}
