<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\HelperController as Helper;
use App\Models\Region;
use JWTAuth;
use Validator;

class RegionController extends Controller
{
    /**
     * Display all provinsi data.
     *
     * @return \Illuminate\Http\Response
     */
    public function getAllProvinsi(){
        $data = Region::all();

        return response()->json([
            'status' => 'Success',
            'data' => $data
        ], 200);
    }

    /**
     * Store new provinsi data
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, Helper $helper){
        $validator = Validator::make($request->all(), [
            'nama_region'  => 'bail|required',
            'id_user'      => 'bail|required|exists:users,id'
        ]);

        if($validator->fails()){
            $errors = $helper->compact($validator->getMessageBag()->toArray());
            return response()->json([
                'status'  => 'Error',
                'message' => $errors
            ], 400);
        }

        $region = Region::create([
            'nama_region' => $request->nama_region,
            'id_user'     => $request->id_user
        ]);

        return response()->json([
            'status' => 'Success',
            'data'   => $region
        ], 200);
    }

    /**
     * Get single provinsi data
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id){
        $region = Region::find($id);

        //jika tidak ada data ditemukan
        if(empty($region)) return response()->json([
            'status' => 'Error',
            'message' => 'Data region tidak ditemukan'
        ], 403);
        
        return response()->json([
            'status' => 'Success',
            'data' => $region
        ], 200);
    }

    /**
     * Update provinsi data.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Helper $helper, $id){
        $validator = Validator::make($request->all(), [
            'nama_region'  => 'bail|required',
            'id_user'      => 'bail|required|exists:users,id'
        ]);

        if($validator->fails()){
            $errors = $helper->compact($validator->getMessageBag()->toArray());
            return response()->json([
                'status'  => 'Error',
                'message' => $errors
            ], 400);
        }

        $region = Region::find($id);

        //jika tidak ada data ditemukan
        if(empty($region)) return response()->json([
            'status' => 'Error',
            'message' => 'Gagal update data region'
        ], 403);

        $region->nama_region = $request->nama_region;
        $region->id_user = $request->id_user;
        $region->save();

        return response()->json([
            'status' => 'Success',
            'data' => $region
        ], 200);
    }

    /**
     * Delete region data.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id){
        $region = Region::find($id);

        //jika provinsi tidak ditemukan
        if(empty($region)) return response()->json([
            'status' => 'Error',
            'message' => 'Data region gagal dihapus'
        ], 403);

        $region->delete();

        return response()->json([
            'status' => 'Success',
            'message' => 'Data region berhasil dihapus'
        ], 200);
    }

     /**
     * Get region by user.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function getRegionByUser($id){
        $region = Region::where('id_user', $id)->get();

        return response()->json([
            'status' => 'Success',
            'data' => $region
        ], 200);
    }

    /**
     * Get kabupaten by region.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function getKabupatenByRegion($id){
        $region = Region::find($id);

        return response()->json([
            'status' => 'Success',
            'data' => $region->kabupaten
        ], 200);
    }

    /**
     * Store region & kabupaten in pivot table.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function syncRegion(Request $request, $id){
        $region = Region::find($id);

        foreach($request->kabupaten as $kabupaten){
            $data[] = $kabupaten;
        }

        $region->kabupaten()->sync($data);

        return response()->json([
            'status' => 'Success',
            'data' => $region->kabupaten
        ], 200);
    }
}
