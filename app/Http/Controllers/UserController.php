<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\HelperController as Helper;
use App\Http\Controllers\FileController as File;
use Illuminate\Validation\Rule;
use App\User;
use App\Models\Profile;
use JWTAuth;
use Validator;

class UserController extends Controller
{
    /**
     * Get all user data
     *
     * @param  \Illuminate\Http\Request  (page, limit, orderby = name/phone/address/email, orderDir = asc/desc, keyword)
     * @return \Illuminate\Http\Response
     */
    public function getAllUser(Request $request){
        $page = $request->page ?: 1;
        $limit = $request->limit ?: 10;
        $orderBy = $request->orderby ?: 'id';
        $orderDir = $request->orderdir ?: 'desc';
        $keyword = $request->keyword ?: null;
        $role = $request->role ?: null;

        $offset = ($page - 1) * $limit;

        $field = ['id', 'username', 'fullname', 'address', 'phone', 'email'];
        $dir = ['asc', 'desc'];

        if(!in_array($orderBy, $field) || !in_array($orderDir, $dir)) return response()->json([
            'status' => 'Error',
            'message' => 'Kesalahan parameter value'
        ], 404);

        $user = User::select('users.*', 'pp.fullname as parent_fullname', 'up.fullname', 'up.address', 'up.phone', 'up.email', 'role_name')
                ->leftJoin('profiles as up', 'users.id', '=', 'up.id_user')
                ->leftJoin('roles', 'users.id_role', '=', 'roles.id')
                ->leftJoin('profiles as pp', 'users.parent', '=', 'pp.id_user')
                ->when(($keyword != null), function($query) use ($keyword){
                    return $query->where('fullname', 'like', "%$keyword%")
                                ->orWhere('username', 'like', "%$keyword%")
                                ->orWhere('address', 'like', "%$keyword%")
                                ->orWhere('phone', 'like', "%$keyword%")
                                ->orWhere('email', 'like', "%$keyword%");
                })
                ->when(($role != null), function($query) use ($role){
                    $roleArr = explode(',', $role);
                    return $query->whereIn('role_name', $roleArr);
                })
                ->orderBy('users.'.$orderBy, $orderDir)
                ->offset($offset)->limit($limit)
                ->get();

        $totalData = User::count();
          
        return response()->json([
            'status' => 'Success',
            'total_data' => $totalData,
            'data' => $user
        ], 200);
    }

    /**
     * Store new user data.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, File $file, Helper $helper)
    {
        $validator = Validator::make($request->all(), [
            'username' => 'bail|required|unique:users,username',
            'password' => 'bail|required',
            'role'     => 'bail|required',
            'fullname' => 'bail|required',
            'address'  => 'bail|required',
            'phone'    => 'bail|required|unique:profiles,phone',
            'email'    => 'bail|required|email|unique:profiles,email',
            'file'     => 'bail|max:2000|file|image|mimes:jpeg,jpg,bmp,png'
        ]);

        if($validator->fails()){
            $errors = $helper->compact($validator->getMessageBag()->toArray());
            return response()->json([
                'status'  => 'Error',
                'message' => $errors
            ], 400);
        }

        DB::beginTransaction();
        try{
            $user = new user;
            $user->username = $request->username;
            $user->password = bcrypt($request->password);
            $user->id_role = $request->role;
            $user->parent = $request->parent;
            $user->badge = $request->badge;

            if($request->hasFile('file')){
                //upload storage
                $photo = $file->upload([$request->file], 'user');
                $user->photo = $photo['image0'];
            }

            $user->save();

            $profile = Profile::create([
                'fullname' => $request->fullname,
                'address'  => $request->address,
                'phone'    => $request->phone,
                'email'    => $request->email,
                'id_user'  => $user->id
            ]);

            $user['profile'] = $profile;
            
            DB::commit();

            return response()->json([
                'status' => 'Success',
                'data' => $user
            ], 200);

        }catch(\Exception $e){
            DB::rollback();

            $data = [
                ["photo_name" => $photo['image0']]
            ];
            $file->delete( $data );

            return response()->json([
                'status' => 'Error',
                'message' => 'Failed to input new user data'
            ], 400);
        }
    }

    /**
     * Get single user data by id user
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id){
        $user = User::with('profile', 'role')->find($id);

        //jika tidak ada data ditemukan
        if(empty($user)) return response()->json([
            'status' => 'Error',
            'message' => 'User not found'
        ], 403);
        
        return response()->json([
            'status' => 'Success',
            'data' => $user
        ], 200);
    }

    /**
     * Update user data.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, File $file, Helper $helper, $id){
        $profile = Profile::where('id_user', $id)->first();

        $validator = Validator::make($request->all(), [
            'username' => ['bail', 'required', Rule::unique('users')->ignore($id)],
            //'role'     => 'bail|required',
            //'parent'   => 'bail|required',
            'fullname' => 'bail|required',
            'address'  => 'bail|required',
            'phone'    => ['bail', 'required', Rule::unique('profiles')->ignore($profile->id)],
            'email'    => ['bail', 'required', 'email', Rule::unique('profiles')->ignore($profile->id)],
            'file'     => 'bail|max:2000|file|image|mimes:jpeg,jpg,bmp,png'
        ]);

        if($validator->fails()){
            $errors = $helper->compact($validator->getMessageBag()->toArray());
            return response()->json([
                'status'  => 'Error',
                'message' => $errors
            ], 400);
        }

        //jika user yg meng-update data adalah admin
        //atau si pemilik akun, izinkan update 
        //if($this->userAuth->level == 1 || $this->userAuth->id == $id){
            $user = User::find($id);

            DB::beginTransaction();

            try{
                $user->username = $request->username;
                $user->parent = $request->parent;
                $user->badge = $request->badge;

                if($request->id_role) $user->id_role = $request->id_role;

                if($request->hasFile('file')){
                    //upload storage
                    $photo = $file->upload([$request->file], 'user');
                    
                    if($user->photo != NULL){
                        //remove foto lama
                        $data = [ ["photo_name" => $user->photo] ];
                        $file->delete( $data );
                    }

                    $user->photo = $photo['image0'];
                }

                $user->save();

                $profile->fullname = $request->fullname;
                $profile->address = $request->address;
                $profile->phone = $request->phone;
                $profile->email = $request->email;
                $profile->save();

                $user['profile'] = $profile;

                DB::commit();

                return response()->json([
                    'status' => 'Success',
                    'data' => $user
                ], 200);

            }catch(\Exception $e){
                DB::rollback();

                return response()->json([
                    'status' => 'Error',
                    'message' => 'Failed to update user'
                ], 400);
            }
        //}

        // return response()->json([
        //     'status' => 'Error',
        //     'messsage' => 'Failed to update user'
        // ], 403);
    }

    /**
     * Delete user data.
     *
     * @param  int  $id
     * @param  string  $forceDelete  (destroy)
     * @return \Illuminate\Http\Response
     */
    public function destroy(File $file, $id){
        //jika user adalah admin, izinkan hapus
        //if($this->userAuth->level == 1){
            $user = User::find($id);

            //jika user tidak ditemukan, maka gagal
            if(empty($user)) return response()->json([
                'status' => 'Error',
                'message' => 'Failed to delete user'
            ], 403);

            // if($user->photo != NULL){
            //     //remove foto lama
            //     $data = [ ["photo_name" => $user->photo] ];
            //     $file->delete( $data );
            // }
            $user->delete();

            return response()->json([
                'status' => 'Success',
                'message' => 'User successfully deleted'
            ], 200);
        //}

        // return response()->json([
        //     'status' => 'Error',
        //     'message' => 'You have no authorization to delete user'
        // ], 403);
    }

    /**
     * Permanently delete user data.
     *
     * @param  int  $id
     * @param  string  $forceDelete  (destroy)
     * @return \Illuminate\Http\Response
     */
    public function forceDelete(File $file, $id){
        //jika user adalah admin, izinkan hapus
        //if($this->userAuth->level == 1){
            $user = User::withTrashed()->find($id);

            if(empty($user)) return response()->json([
                'status' => 'Error',
                'message' => 'Failed to delete user'
            ], 403);

            if($user->photo != NULL){
                //remove foto lama
                $data = [ ["photo_name" => $user->photo] ];
                $file->delete( $data );
            }
            $user->forceDelete();

            return response()->json([
                'status' => 'Success',
                'message' => 'User successfully deleted'
            ], 200);
        //}

        // return response()->json([
        //     'status' => 'Error',
        //     'message' => 'You have no authorization to delete user'
        // ], 403);
    }
}
