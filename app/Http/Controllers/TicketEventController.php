<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\TicketEvent as Ticket;
use Validator;
use App\Http\Controllers\HelperController as Helper;
use App\Http\Controllers\EmailController as Email;

class TicketEventController extends Controller
{
    /**
     * Store new ticket data
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, Helper $helper, Email $email){
        //return $request->all();
        $errors = $this->validation($request, $helper);
        
        //jika ada error pd inputan user
        if(!empty($errors)) return response()->json([
            'status'  => 'Error',
            'message' => $errors
        ], 400);

        $ticket = Ticket::create([
            'title'       => $request->json('title'),
            'id_customer' => $request->json('id_customer'),
            'id_event'    => $request->json('id_event')
        ]);

        //$email->ticketCreatedEmail($ticket);

        return response()->json([
            'status' => 'Success',
            'data' => $ticket
        ], 200);
    }

    /**
     * Get single ticket data
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
     public function show($id){
        $ticket = Ticket::find($id);

        //jika tidak ada data ditemukan
        if(empty($ticket)) return response()->json([
            'status' => 'Error',
            'message' => 'Ticket data not found'
        ], 403);
        
        return response()->json([
            'status' => 'Success',
            'data' => $ticket
        ], 200);
    }
    /**
     * Delete ticket data.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
     public function destroy($id){
        //jika user adalah admin, izinkan hapus
        //if($this->userAuth->level == 1){
            $ticket = Ticket::find($id);

            //jika ticket tidak ditemukan
            if(empty($ticket)) return response()->json([
                'status' => 'Error',
                'message' => 'Failed to delete ticket'
            ], 403);

            $ticket->delete();

            return response()->json([
                'status' => 'Success',
                'message' => 'Successfully delete ticket'
            ], 200);
        //}

        // return response()->json([
        //     'status' => 'Error',
        //     'message' => 'Anda tidak memiliki otorisasi untuk menghapus provinsi'
        // ], 403);
    }

    /**
     * Get ticket by user.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @param  string  $role
     * @return \Illuminate\Http\Response
     */
    public function getTicketByUser(Request $request, $role, $id){
        $ticket = Ticket::when(($role == 'vendor'), function($query) use ($id){
            return $query->where('id_vendor', $id);
        }, function($query) use ($id){
            return $query->where('id_customer', $id);
        })->get();
        
        return response()->json([
            'status' => 'Success',
            'data' => $ticket
        ], 200);
    }

    public function close(Request $request){
        $id = $request->json('id_ticket');
        $tiket = Ticket::find($id);
        $tiket->status = 0;
        $tiket->save();
        
        return response()->json([
            'status' => 'Success',
            'data' => 'Tiket Telah Ditutup'
        ], 200);
    }

    /**
     * Validate user input.
     *
     * @param  object  $request
     * @param  object  $helper
     * @return array
     */
     public function validation($request, $helper){
        $validator = Validator::make($request->all(), [
            'title'       => 'bail|required',
            'id_customer' => 'bail|required|exists:users,id',
            'id_event'    => 'bail|required|exists:events,id'
        ]);

        if($validator->fails()){
            return $helper->compact($validator->getMessageBag()->toArray());
        }
    }
}
