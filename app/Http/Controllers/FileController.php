<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Youtube;

class FileController extends Controller
{
    /**
     * Upload file to storage
     *
     * @param  array  $file
     * @param  string  $type
     * @return array
     */
    public function upload($file, $type){
        $year = date('Y');
        
        foreach($file as $key => $val){
            $path = $val->store("images/$type/$year");
            $name = str_replace('/', '-', $path);

            $data["image{$key}"] = $name;
        }

        return $data;
    }

    /**
     * Download file from storage
     *
     * @param  string  $path
     * @return file
     */
    public function download($path){
        $path = str_replace('-', '/', $path);
        //return response()->download(public_path("storage/$path"));
        return response()->file(storage_path("app/public/$path"));
    }

    /**
     * Delete file from storage
     *
     * @param  array  $file
     * @return void
     */
    public function delete($file){
        if(is_object($file)) $file = json_decode($file, true);

        foreach($file as $value){
            $name = str_replace('-', '/', $value['photo_name']);
            $photo[] = $name;
        }

        return Storage::delete($photo);
    }

    /**
     * Upload video youtube
     *
     * @param  \App\Http\Controllers\FileController  $file
     * @return \Illuminate\Http\Response
     */
    public function uploadVideo($file){
        $filename = pathinfo($file->getClientOriginalName(), PATHINFO_FILENAME);

        $video = Youtube::upload($file, [
            'title'       => $filename,
            'description' => 'Billboard Video.',
            'tags'	      => ['foo', 'bar', 'baz'],
            'category_id' => 10
        ], 'unlisted');
        
        return $video->getVideoId();
    }

    /**
     * Delete file from storage
     *
     * @param  array  $video
     * @return void
     */
    public function deleteVideo($video){
        foreach($video as $vid){
            Youtube::delete($vid->video_name);
        }
    }
}
