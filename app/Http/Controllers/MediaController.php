<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Photo;
use App\Models\Video;
use App\Http\Controllers\FileController as File;

class MediaController extends Controller
{
    /**
     * Delete foto permanent
     *
     * @param  int  $id
     * @param  \App\Http\Controllers\FileController  $file
     * @return \Illuminate\Http\Response
     */
    public function deletePhoto(File $file, $id){
        $photo = Photo::find($id);

        if(empty($photo)) return response()->json([
            'status' => 'Error',
            'message' => 'Failed to delete photo'
        ], 403);

        $photo_name = [
            ['photo_name' => $photo->photo_name]
        ];

        $photo->delete();
        $file->delete($photo_name);

        return response()->json([
            'status' => 'Success',
            'message' => 'Photo successfully deleted'
        ], 200);
    }

    /**
     * Delete video permanent
     *
     * @param  int  $id
     * @param  \App\Http\Controllers\FileController  $file
     * @return \Illuminate\Http\Response
     */
    public function deleteVideo(File $file, $id){
        $video = Video::find($id);

        if(empty($video)) return response()->json([
            'status' => 'Error',
            'message' => 'Failed to delete photo'
        ], 403);

        $file->deleteVideo([$video]);
        $video->delete();

        return response()->json([
            'status' => 'Success',
            'message' => 'Photo successfully deleted'
        ], 200);
    }
}
