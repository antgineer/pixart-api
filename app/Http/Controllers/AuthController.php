<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use JWTAuth;
use Tymon\JWTAuth\Exceptions\JWTException;
use Illuminate\Support\Facades\Hash;
use App\Http\Controllers\HelperController as Helper;
use App\User;
use Validator;

class AuthController extends Controller
{
    /**
     * Authentication login
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function authentication(Request $request){
        $cres = $request->only('username', 'password');

        try{
            //validasi cresedential dan membuat token
            if(!$token = JWTAuth::attempt($cres)){
                return response()->json([
                    'status' => 'Error',
                    'message' => 'Invalid Cresedential, Email atau Password salah'
                ], 401);
            }
        }catch(JWTException $e){
            //jika ada error
            return response()->json([
                'status' => 'Error',
                'message' => 'Gagal membuat token'
            ], 500);
        }

        $user = User::select('users.id', 'username', 'email', 'fullname', 'users.id_role', 'role_name', 'photo')
                ->leftJoin('roles', 'users.id_role', '=', 'roles.id')
                ->leftJoin('profiles', 'users.id', '=', 'profiles.id_user')
                ->where('username', $request->username)->first();

        //$token = JWTAuth::fromUser($user);

        // $tokens = JWTAuth::getPayload($token);
        // return $tokens->get('exp');

        return response()->json([
            'status' => 'Success',
            'data'   => $user,
            'token'  => $token
        ], 200);
    }

    /**
     * Refresh token
     *
     * @return \Illuminate\Http\Response
     */
    public function refreshToken(Request $request){
        $token = JWTAuth::getToken();
        $newToken = JWTAuth::refresh($token);
        
        return response()->json([
            'status' => 'Success',
            'token'  => $newToken
        ], 200);
    }

    /**
     * Change user password
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function changePassword(Request $request, Helper $helper){
        $validator = Validator::make($request->all(), [
            'current_password' => 'bail|required',
            'new_password'     => 'bail|required'
        ]);

        if($validator->fails()){
            $errors = $helper->compact($validator->getMessageBag()->toArray());
            return response()->json([
                'status'  => 'Error',
                'message' => $errors
            ], 400);
        }

        $current_password = $request->current_password;
        $user_auth = JWTAuth::parseToken()->authenticate();

        //cek apakah password sekarang valid
        //jika valid, ganti password
        if(Hash::check($current_password, $user_auth->password)) {
            $new_password = Hash::make($request->new_password);
            $user = User::find($user_auth->id);
            $user->password = $new_password;
            $user->save();

            return response()->json([
                'status' => 'Success',
                'message' => 'Password berhasil diubah'
            ], 200);
        }
        else{
            return response()->json([
                'status' => 'Error',
                'message' => 'Password salah'
            ], 403);
        }
    }

    /**
     * Logout
     *
     * @return \Illuminate\Http\Response
     */
    public function logout(){
        //invalidate token
        $token = JWTAuth::getToken();
        JWTAuth::invalidate($token);

        return response()->json([
            'status' => 'Success',
            'message' => 'Berhasil Logout'
        ], 200);
    }
}
