<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Billboard;
use App\Models\Contract;
use App\User;
use Illuminate\Support\Facades\DB;

class StatisticController extends Controller
{
    public function countAllBillboard(){
        $billboard = Billboard::where('verify', '!=', '0')->count();

        return $billboard;
    }

    public function countBillboard($id, $status){
        //unefficient
        $billboard = Billboard::leftJoin('contracts', 'billboards.id', '=', 'contracts.id_billboard')
        ->where('id_user', $id)
        ->when(($status == 'occupied'), function($query){
            return $query->where('expired_date', '>=', DB::raw('CURDATE()'));
        })
        ->when(($status == 'unoccupied'), function($query){
            return $query->groupBy('billboards.id')
                         ->havingRaw('ifnull(max(expired_date), (CURDATE() - INTERVAL 1 DAY))  < CURDATE()');
        })
        ->when(($status == 'all'), function($query){
            return $query->groupBy('billboards.id');
        })
        ->get()->count();

        return $billboard;
    }

    public function countContract($id, $status){
        $contract = Contract::with([
            'detailContract' => function($query){
                return $query->with(['photo' => function($query){
                    $query->select('photo_name', 'photo_id');
                }])
                ->latestDetailContract();
            }
        ])
        ->where('id_customer', $id)
        ->when(($status == 'occupied'), function($query){
            return $query->where('expired_date', '>=', DB::raw('CURDATE()'));
        })
        ->when(($status == 'contractexpired'), function($query){
            return $query->where('expired_date', '<', DB::raw('CURDATE()'));
        })
        ->when(($status == 'willexpired'), function($query){
            return $query->where(function($query){
                return $query->where('expired_date', '>=', DB::raw('CURDATE()'))
                             ->where('expired_date', '<=', DB::raw('(CURDATE() + INTERVAL 3 MONTH)'));
            });
        })
        ->when(($status == 'newupdated'), function($query){
            return $query->whereHas('detailContract', function($query){
                return $query->where(DB::raw('DATE(created_at)'), DB::raw('CURDATE()'));
            });
        })
        ->when(($status == 'detailexpired'), function($query){
            return $query->where('expired_date', '>=', DB::raw('CURDATE()'))
            ->whereHas('detailContract', function($query){
                return $query->select('id', DB::raw('MAX(expired_date) AS exp'), 'id_contract')
                ->groupBy('id_contract')
                ->having('exp', '<', DB::raw('CURDATE()'));
            });
        })
        ->get()->count();

        return $contract;
    }

    public function countUser($role){
        $user = User::whereHas('role', function($query) use ($role){
            return $query->where('role_name', $role);
        })->count();

        return $user;
    }

    public function countContractExpired($role, $id){
        $contract = Contract::select(DB::raw("MONTH(expired_date) AS bulan"), DB::raw("YEAR(expired_date) AS tahun"), DB::raw("COUNT(id) AS total_kontrak"))
        ->where('expired_date', '>=', DB::raw('LAST_DAY(CURDATE() - INTERVAL 1 MONTH) + INTERVAL 1 DAY'))
        ->where('expired_date', '<=', DB::raw('LAST_DAY(CURDATE() + INTERVAL 11 MONTH)'))
        ->when($role == 'vendor', function($query) use ($id){
            return $query->where('id_vendor', $id);
        }, function($query) use ($id){
            return $query->where('id_customer', $id);
        })
        ->groupBy('tahun', 'bulan')
        ->orderBy('tahun')
        ->get();

        //return $contract;

        // if(count($contract) <= 0) return response()->json([
        //     'status' => 'Error',
        //     'message' => 'Cannot show contract data'
        // ], 404);

        // $date = (int) date('n');
        // $year = (int) date('Y');

        // for($i = 1; $i <= 12; $i++){
        //     $data[] = [
        //         'bulan' => $date,
        //         'tahun' => $year,
        //         'total_kontrak' => 0
        //     ];

        //     if($date != 12) {
        //         $date++;
        //     }
        //     else{
        //         $date = 1;
        //         $year++;
        //     }
        // }

        // $dataMerge = array_merge($data, $contract->toArray());

        // $result = [];
        // foreach($dataMerge as $key => $val){
        //     //
        //     if( !array_key_exists($val['bulan'], $result) || $result[$val['bulan']]['total_kontrak'] < 1 )
        //         $result[$val['bulan']] = $val;
        // }
        // return $result;

        //return $data;

        return response()->json([
            'status' => 'Success',
            'data'   => $contract
        ], 200);
    }

    public function countContractByProvinsi($role, $id){
        $user = $role == 'vendor' ? 'contracts.id_vendor' : 'contracts.id_customer';

        $contract = Contract::select([
            DB::raw("LOWER(REPLACE(provinsis.nama_provinsi, ' ', '')) AS kode_provinsi"),
            'nama_provinsi', 
            DB::raw('COUNT(contracts.id) AS total_kontrak')
        ])
        ->rightJoin('billboards', 'contracts.id_billboard', '=', DB::raw("billboards.id AND $user = :id AND expired_date >= CURDATE()"))
        ->rightJoin('provinsis', 'billboards.id_provinsi', '=', 'provinsis.id')
        ->groupBy('provinsis.id')
        ->setBindings(['id' => $id])
        ->get();

        return response()->json([
            'status' => 'Success',
            'data'   => $contract
        ], 200);
    }

    public function countContractByRegion($id){
        $contract = Contract::select([
            DB::raw("LOWER(REPLACE(regions.nama_region, ' ', '')) AS kode_region"),
            'nama_region', 
            DB::raw('COUNT(contracts.id) AS total_kontrak')
        ])
        ->rightJoin('billboards', 'contracts.id_billboard', '=', DB::raw("billboards.id AND contracts.id_customer = ? AND expired_date >= CURDATE()"))
        ->rightJoin('kabupatens', 'billboards.id_kabupaten', '=', 'kabupatens.id')
        ->rightJoin('region_kabupaten', 'kabupatens.id', '=', 'region_kabupaten.id_kabupaten')
        ->rightJoin('regions', 'region_kabupaten.id_region', '=', 'regions.id')
        ->where('regions.id_user', $id)
        ->groupBy('regions.id')
        ->addBinding($id)
        ->get();

        return response()->json([
            'status' => 'Success',
            'data'   => $contract
        ], 200);
    }

    public function topUser($role, $id){
        $contract = Contract::when($role == 'vendor', function($query) use ($id){
            return $query->select('id_customer', 'customer_name', DB::raw('COUNT(id) AS total_kontrak'))
                    ->with('customer.profile')
                    ->where('id_vendor', $id)
                    ->groupBy('id_customer');
        }, function($query) use ($id){
            return $query->select('id_vendor', DB::raw('COUNT(id) AS total_kontrak'))
                    ->with('vendor.profile')
                    ->where('id_customer', $id)
                    ->groupBy('id_vendor');
        })
        ->orderBy('total_kontrak', 'desc')
        ->limit(10)
        ->get();

        return response()->json([
            'status' => 'Success',
            'data'   => $contract
        ], 200);
    }

    public function adminStatistic(){
        $totalVendor = $this->countUser('vendor');
        $totalCustomer = $this->countUser('customer');
        $totalBillboard = $this->countAllBillboard();

        $user = [
            'total_vendor' => $totalVendor,
            'total_customer' => $totalCustomer,
            'total_billboard' => $totalBillboard
        ];

        return response()->json([
            'status' => 'Success',
            'data'   => $user
        ], 200);
    }
    
    public function vendorStatistic($id){
        $totalBillboard = $this->countBillboard($id, 'all');
        $occupiedBillboard = $this->countBillboard($id, 'occupied');
        $unoccupiedBillboard = $this->countBillboard($id, 'unoccupied');

        $billboard = [
            'total_billboard' => $totalBillboard,
            'occupied_billboard' => $occupiedBillboard,
            'unoccupied_billboard' => $unoccupiedBillboard
        ];

        return response()->json([
            'status' => 'Success',
            'data'   => $billboard
        ], 200);
    }

    public function customerStatistic($id){
        $occupiedBillboard = $this->countContract($id, 'occupied');
        $newupdatedBillboard = $this->countContract($id, 'newupdated');
        $contractexpiredBillboard = $this->countContract($id, 'contractexpired');
        $willexpiredBillboard = $this->countContract($id, 'willexpired');
        $detailexpiredBillboard = $this->countContract($id, 'detailexpired');

        $contract = [
            'contract_active' => $occupiedBillboard,
            'contract_expired' => $contractexpiredBillboard,
            'contract_will_expired' => $willexpiredBillboard,
            'detail_contract_expired' => $detailexpiredBillboard,
            'contract_new_updated' => $newupdatedBillboard
        ];

        return response()->json([
            'status' => 'Success',
            'data'   => $contract
        ], 200);
    }
}
