<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\HelperController as Helper;
use App\Models\Kabupaten;
use App\Models\Kecamatan;
use JWTAuth;
use Validator;

class KecamatanController extends Controller
{
    /**
     * Attribute auth user.
     *
     * @var array
     */
    //private $userAuth;

    /**
     * Constructor.
     *
     * @return void
     */
    // public function __construct(){
    //     $this->userAuth = JWTAuth::parseToken()->authenticate();
    // }

    /**
     * Get all kecamatan data
     *
     * @param  int  $page
     * @param  int  $limit
     * @param  string  $orderBy  (name)
     * @param  string  $orderDir  (asc, desc)
     * @param  string  $keyword
     * @return \Illuminate\Http\Response
     */
     public function getAllKecamatan($page = 1, $limit = 10, $orderBy = 'id', $orderDir = 'asc', $keyword = null){
        $offset = ($page - 1) * $limit;
        $orderDir = 'desc'; //sementara
        
        $kecamatan = kecamatan::when(($keyword != null), function($query) use ($keyword){
            return $query->where('nama_kecamatan', 'like', "%$keyword%");
        })->orderBy($orderBy, $orderDir)
          ->offset($offset)->limit($limit)
          ->get();

        $totalData = Kecamatan::count();
          
        return response()->json([
            'status' => 'Success',
            'total_data' => $totalData,
            'data' => $kecamatan
        ], 200);
    }

    /**
     * Get all kecamatan by kabupaten
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function getKecamatanByKabupaten($id){
        $data = Kecamatan::where('id_kabupaten', $id)->get();

        //jika tidak ada data ditemukan
        if(count($data) == 0) return response()->json([
            'status' => 'Error',
            'message' => 'Data kecamatan tidak ditemukan'
        ], 200);

        return response()->json([
            'status' => 'Success',
            'data' => $data
        ], 200);
    }

    /**
     * Store new kecamatan data
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, Helper $helper){
        $validator = Validator::make($request->all(), [
            'id_kabupaten'   => 'bail|required',
            'nama_kecamatan' => 'bail|required'
        ]);

        if($validator->fails()){
            $errors = $helper->compact($validator->getMessageBag()->toArray());
            return response()->json([
                'status'  => 'Error',
                'message' => $errors
            ], 400);
        }

        //jika user adalah admin, izinkan tambah data
        //if($this->userAuth->level == 1){
            $kab = Kabupaten::find($request->id_kabupaten);

            //jika tidak ada data kabupaten tujuan, gagal
            if(empty($kab)) return response()->json([
                'status' => 'Error',
                'message' => 'Gagal menyimpan data kecamatan'
            ], 403);

            $kec = Kecamatan::create([
                'id_kabupaten'        => $request->id_kabupaten,
                'nama_kecamatan'      => $request->nama_kecamatan,
                'nama_kecamatan_gmap' => $request->nama_kecamatan_gmap
            ]);

            return response()->json([
                'status' => 'Success',
                'data' => $kec
            ], 200);
        //}

        // return response()->json([
        //     'status' => 'Error',
        //     'message' => 'Anda tidak memiliki otorisasi untuk menambah kecamatan'
        // ], 403);
    }

    /**
     * Get single kecamatan data
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id){
        $kec = Kecamatan::with('kabupaten')->find($id);

        //jika tidak ada data ditemukan
        if(empty($kec)) return response()->json([
            'status' => 'Error',
            'message' => 'Data kecamatan tidak ditemukan'
        ], 403);
        
        return response()->json([
            'status' => 'Success',
            'data' => $kec
        ], 200);
    }

    /**
     * Update kecamatan data.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Helper $helper, $id){
        $validator = Validator::make($request->all(), [
            'id_kabupaten'   => 'bail|required',
            'nama_kecamatan' => 'bail|required'
        ]);

        if($validator->fails()){
            $errors = $helper->compact($validator->getMessageBag()->toArray());
            return response()->json([
                'status'  => 'Error',
                'message' => $errors
            ], 400);
        }

        //jika user yg meng-update data adalah admin, izinkan update 
        //if($this->userAuth->level == 1){
            $kec = Kecamatan::find($id);

            //jika tidak ada data ditemukan
            if(empty($kec)) return response()->json([
                'status' => 'Error',
                'message' => 'Gagal update data kecamatan'
            ], 403);

            $kec->id_kabupaten = $request->id_kabupaten;
            $kec->nama_kecamatan = $request->nama_kecamatan;
            $kec->nama_kecamatan_gmap = $request->nama_kecamatan_gmap;
            $kec->save();

            return response()->json([
                'status' => 'Success',
                'data' => $kec
            ], 200);
        //}

        // return response()->json([
        //     'status' => 'Error',
        //     'messsage' => 'Anda tidak memiliki otorisasi untuk mengubah kecamatan'
        // ], 403);
    }

    /**
     * Delete kecamatan data.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id){
        //jika user adalah admin, izinkan hapus
        //if($this->userAuth->level == 1){
            $kec = Kecamatan::find($id);

            //jika user tidak ditemukan
            if(empty($kec)) return response()->json([
                'status' => 'Error',
                'message' => 'Data kecamatan gagal dihapus'
            ], 403);

            $kec->delete();

            return response()->json([
                'status' => 'Success',
                'message' => 'Data kecamatan berhasil dihapus'
            ], 200);
        //}

        // return response()->json([
        //     'status' => 'Error',
        //     'message' => 'Anda tidak memiliki otorisasi untuk menghapus kecamatan'
        // ], 403);
    }
}
