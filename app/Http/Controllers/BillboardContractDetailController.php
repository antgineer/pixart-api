<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\BillboardContractDetail as ConDet;
use Validator;
use App\Models\Photo;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\FileController as File;
use App\Http\Controllers\HelperController as Helper;
use App\Http\Controllers\EmailController as Email;

class BillboardContractDetailController extends Controller
{
    /**
     * Store new contract detail
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  App\Http\Controllers\HelperController  $helper
     * @param  \App\Http\Controllers\FileController  $file
     * @return \Illuminate\Http\Response
     */
     public function store(Request $request, Helper $helper, File $file, Email $email){
        $errors = $this->validation($request, $helper);
        
        //jika ada error pd inputan user
        if(!empty($errors)) return response()->json([
            'status'  => 'Error',
            'message' => $errors
        ], 400);

        //ambil id user pemilik billboard
        //$billboard = Billboard::select('id_user')->find($request->id_billboard);
        
        //jika user adalah admin
        //atau pemilik billboard, izinkan buat contract
        //if($this->userAuth->level == 1 || $this->userAuth->id == $billboard->id_user){

            //update status kontrak yg lain jd 0
            ConDet::where('status', '1')->where('id_contract', $request->id_contract)
            ->update(['status' => '0']);

            //create detail kontrak
            $condet = ConDet::create([
                'expired_date' => $request->expired_date,
                'id_contract'  => $request->id_contract,
                'id_user'      => $request->id_user,
                'tema_iklan'   => $request->tema_iklan,
                'brand'        => $request->brand,
                'status'       => '1'
            ]);

            if($request->hasFile('file')){
                //upload storage
                $photo = $file->upload($request->file, 'billboard_contract');

                foreach($photo as $name){
                    //insert to photo table
                    $image = Photo::create([
                        'photo_name' => $name,
                        'photo_id'   => $condet->id,
                        'photo_type' => 'billboard_contract'
                    ]);
                }
            }

            $email->contractDetailCreatedEmail($condet);

            $condet = collect($condet, $condet->photo);

            return response()->json([
                'status' => 'Success',
                'data'   => $condet
            ], 200);
        //}

        // return response()->json([
        //     'status'  => 'Error',
        //     'message' => 'You have no authorization to add new contract'
        // ], 403);
    }

    /**
     * Get single detail kontrak
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
     public function show($id){
        $condet = ConDet::select([ 
            'billboard_contract_details.id', 
            'billboard_contract_details.tema_iklan', 
            'billboard_contract_details.brand', 
            'billboard_contract_details.status',
            DB::raw('DATE(billboard_contract_details.created_at) AS created_date'), 
            DB::raw('DATE(billboard_contract_details.expired_date) AS expired_date'), 
            'billboard_contract_details.id_contract', 
            'billboard_contract_details.id_user',
            DB::raw('(select id from billboard_contract_details AS bcd1 
            WHERE bcd1.id < billboard_contract_details.id 
            AND bcd1.id_contract = billboard_contract_details.id_contract
            ORDER BY id DESC LIMIT 1) AS previous_id'),
            DB::raw('(select id from billboard_contract_details AS bcd2 
            WHERE bcd2.id > billboard_contract_details.id
            AND bcd2.id_contract = billboard_contract_details.id_contract
            ORDER BY id LIMIT 1) AS next_id')
        ])
        ->with(['photo' => function($query){
            $query->select('id', 'photo_name', 'photo_id');
        }])->find($id);

        //jika tidak ada data ditemukan
        if(empty($condet)) return response()->json([
            'status' => 'Error',
            'message' => 'Detail contract not found'
        ], 403);
        
        return response()->json([
            'status' => 'Success',
            'data' => $condet
        ], 200);
    }

    /**
     * Update detail kontrak.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  App\Http\Controllers\HelperController  $helper
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
     public function update(Request $request, File $file, Helper $helper, $id){
        $errors = $this->validation($request, $helper);
        
        //jika ada error pd inputan user
        if(!empty($errors)) return response()->json([
            'status'  => 'Error',
            'message' => $errors
        ], 400);

        //jika user yg meng-update data adalah admin, izinkan update 
        //if($this->userAuth->level == 1){
            $condet = ConDet::find($id);

            //jika tidak ada data ditemukan
            if(empty($condet)) return response()->json([
                'status' => 'Error',
                'message' => 'Failed to update contract detail'
            ], 403);

            $condet->expired_date = $request->expired_date;
            $condet->id_contract  = $request->id_contract;
            $condet->id_user      = $request->id_user;
            $condet->tema_iklan   = $request->tema_iklan;
            $condet->brand        = $request->brand;
            $condet->save();

            if($request->hasFile('file')){
                //upload storage
                $photo = $file->upload($request->file, 'billboard_contract');

                foreach($photo as $name){
                    //insert to photo table
                    $image = Photo::create([
                        'photo_name' => $name,
                        'photo_id'   => $condet->id,
                        'photo_type' => 'billboard_contract'
                    ]);
                }
            }

            $condet = collect($condet, $condet->photo);

            return response()->json([
                'status' => 'Success',
                'data' => $condet
            ], 200);
        //}

        // return response()->json([
        //     'status' => 'Error',
        //     'messsage' => 'Anda tidak memiliki otorisasi untuk mengubah provinsi'
        // ], 403);
    }

    /**
     * Delete detail kontrak.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
     public function destroy(File $file, $id){
        //jika user adalah admin, izinkan hapus
        //if($this->userAuth->level == 1){
            $condet = ConDet::with(['photo' => function($query){
                $query->select('photo_name', 'photo_id');
            }])->find($id);

            //jika provinsi tidak ditemukan
            if(empty($condet)) return response()->json([
                'status' => 'Error',
                'message' => 'Failed to delete contract detail'
            ], 403);

            $condet->delete();

            //jika billboard punya foto
            if(count($condet->photo) > 0){
                //delete data foto & file
                $condet->photo()->where('photo_id', $condet->id)->delete();
                $file->delete($condet->photo);
            }

            return response()->json([
                'status' => 'Success',
                'message' => 'Successfully deleted contract detail'
            ], 200);
        //}

        // return response()->json([
        //     'status' => 'Error',
        //     'message' => 'Anda tidak memiliki otorisasi untuk menghapus provinsi'
        // ], 403);
    }

    /**
     * Get detail kontrak by kontrak
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
     public function getDetailByContract($id){
        $condet = ConDet::with(['photo' => function($query){
            $query->select('id', 'photo_name', 'photo_id');
        }])
        ->where('id_contract', $id)
        ->get();

        //jika tidak ada data ditemukan
        if(empty($condet)) return response()->json([
            'status' => 'Error',
            'message' => 'Detail contract not found'
        ], 403);
        
        return response()->json([
            'status' => 'Success',
            'data' => $condet
        ], 200);
    }

    /**
     * Validate user input.
     *
     * @param  object  $request
     * @param  object  $helper
     * @return array
     */
     public function validation($request, $helper){
        $validator = Validator::make($request->all(), [
            'expired_date' => 'bail|required|date',
            'id_contract'  => 'bail|required|exists:contracts,id',
            'id_user'      => 'bail|required|exists:users,id',
            'tema_iklan'   => 'bail|required',
            'brand'        => 'bail|required',
            'file.*'       => 'bail|max:2000|file|image|mimes:jpeg,jpg,bmp,png'
        ]);

        if($validator->fails()){
            return $helper->compact($validator->getMessageBag()->toArray());
        }
    }
}
