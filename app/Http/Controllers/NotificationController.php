<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Notification;

class NotificationController extends Controller
{
    /**
     * Get notification
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function getNotification(Request $request, $id = null){
        $page  = $request->page ?: 1;
        $limit = $request->limit ?: 10;

        $offset = ($page - 1) * $limit;

        //untuk tampilkan yg blm di unread aja
        $unread = $request->unread ?: false;

        $notif = Notification::with(['sender.profile'])
        ->when($id, function($query) use ($id){
            return $query->where('id_user', $id);
        })
        ->when($unread, function($query) use ($id){
            if($id) return $query->where('user_read_status', '0');
            else return $query->where('admin_read_status', '0');
        })
        ->latest()->offset($offset)->limit($limit)
        ->get();

        return response()->json([
            'status' => 'Success',
            'data' => $notif
        ], 200);
    }

    /**
     * Get notification total unread
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function getTotalUnread($id = null){
        $unread = Notification::when($id, function($query) use ($id){
            return $query->where('id_user', $id)->where('user_read_status', '0');
        }, function($query){
            return $query->where('admin_read_status', '0');
        })->count();

        return response()->json([
            'status' => 'Success',
            'total_unread' => $unread
        ], 200);
    }

    /**
     * Update status read user notification
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
     public function updateStatusRead($id, $admin = false){
        $notif = Notification::find($id);
        
        //jika tidak ada data ditemukan
        if(empty($notif)) return response()->json([
            'status' => 'Error',
            'message' => 'Failed to update notification'
        ], 403);

        if($admin){
            $notif->admin_read_status = '1';
        }else{
            $notif->user_read_status = '1';
        }

        $notif->save();

        return response()->json([
            'status' => 'Success',
            'data' => $notif
        ], 200);
     }
}
