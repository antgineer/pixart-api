<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Role;
use App\Http\Controllers\HelperController as Helper;
use Validator;

class RoleController extends Controller
{
    /**
     * Display all role data.
     *
     * @return \Illuminate\Http\Response
     */
     public function getAllRole(){
        $data = Role::all();

        return response()->json([
            'status' => 'Success',
            'data' => $data
        ], 200);
    }

    /**
     * Store new role data
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
     public function store(Request $request, Helper $helper){
        $validator = Validator::make($request->all(), [
            'role_name'  => 'bail|required',
            'harga'      => 'bail|required'
        ]);

        if($validator->fails()){
            $errors = $helper->compact($validator->getMessageBag()->toArray());
            return response()->json([
                'status'  => 'Error',
                'message' => $errors
            ], 400);
        }

        //jika user adalah admin, izinkan tambah data
        //if($this->userAuth->level == 1){
            $role = Role::create([
                'role_name'     => $request->role_name,
                'user_created'  => 'admin',
                'user_modified' => 'admin',
                'harga'         => $request->harga
            ]);

            return response()->json([
                'status' => 'Success',
                'data' => $role
            ], 200);
        //}

        // return response()->json([
        //     'status' => 'Error',
        //     'message' => 'Anda tidak memiliki otorisasi untuk menambah role'
        // ], 403);
    }

    /**
     * Get single role data
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
     public function show($id){
        $role = Role::find($id);

        //jika tidak ada data ditemukan
        if(empty($role)) return response()->json([
            'status' => 'Error',
            'message' => 'Data role tidak ditemukan'
        ], 403);
        
        return response()->json([
            'status' => 'Success',
            'data' => $role
        ], 200);
    }

    /**
     * Update role data.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
     public function update(Request $request, Helper $helper, $id){
        $validator = Validator::make($request->all(), [
            'role_name'  => 'bail|required',
            'harga'      => 'bail|required'
        ]);

        if($validator->fails()){
            $errors = $helper->compact($validator->getMessageBag()->toArray());
            return response()->json([
                'status'  => 'Error',
                'message' => $errors
            ], 400);
        }

        //jika user yg meng-update data adalah admin, izinkan update 
        //if($this->userAuth->level == 1){
            $role = Role::find($id);

            //jika tidak ada data ditemukan
            if(empty($role)) return response()->json([
                'status'  => 'Error',
                'message' => 'Gagal update data role'
            ], 403);

            $role->role_name = $request->role_name;
            $role->harga = $request->harga;
            $role->save();

            return response()->json([
                'status' => 'Success',
                'data' => $role
            ], 200);
        //}

        // return response()->json([
        //     'status' => 'Error',
        //     'messsage' => 'Anda tidak memiliki otorisasi untuk mengubah role'
        // ], 403);
    }

    /**
     * Delete role data.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
     public function destroy($id){
        //jika user adalah admin, izinkan hapus
        //if($this->userAuth->level == 1){
            $role = Role::find($id);

            //jika role tidak ditemukan
            if(empty($role)) return response()->json([
                'status' => 'Error',
                'message' => 'Data role gagal dihapus'
            ], 403);

            $role->delete();

            return response()->json([
                'status' => 'Success',
                'message' => 'Data role berhasil dihapus'
            ], 200);
        //}

        // return response()->json([
        //     'status' => 'Error',
        //     'message' => 'Anda tidak memiliki otorisasi untuk menghapus role'
        // ], 403);
    }

    /**
     * Get module data by role.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function getModuleByRole($id){
        $role = Role::find($id);

        return response()->json([
            'status' => 'Success',
            'data' => $role->module
        ], 200);
    }

    /**
     * Store Role & module in pivot table.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function syncModule(Request $request, $id){
        $role = Role::find($id);

        foreach($request->module as $module){
            $data[] = $module;
        }

        $role->module()->sync($data);

        return response()->json([
            'status' => 'Success',
            'data' => $role->module
        ], 200);
    }

    /**
     * Get single role data by role name
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
     public function getRoleByName($name){
        $role = Role::where('role_name', $name)->first();

        //jika tidak ada data ditemukan
        if(empty($role)) return response()->json([
            'status' => 'Error',
            'message' => 'Data role tidak ditemukan'
        ], 403);
        
        return response()->json([
            'status' => 'Success',
            'data' => $role
        ], 200);
    }
}
