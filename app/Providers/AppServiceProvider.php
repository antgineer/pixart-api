<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Eloquent\Relations\Relation;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Schema::defaultStringLength(191);
        
        Relation::morphMap([
            'billboard'                  => 'App\Models\Billboard',
            'billboard_approved'         => 'App\Models\Billboard',
            'billboard_declined'         => 'App\Models\Billboard',
            'contract'                   => 'App\Models\Contract',
            'contract_expired'           => 'App\Models\Contract',
            'billboard_contract'         => 'App\Models\BillboardContractDetail',
            'billboard_contract_expired' => 'App\Models\BillboardContractDetail',
            'ticket'                     => 'App\Models\Ticket',
            'ticket_content'             => 'App\Models\TicketContent',
            'event'                      => 'App\Models\Event'
        ]);
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
