<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class TicketCreated extends Mailable
{
    public $idContract;
    public $kodeBillboard;
    public $status;
    public $vendor;
    public $customer;
    public $url;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($data)
    {
        $this->idContract    = $data['id_contract'];
        $this->kodeBillboard = $data['kode_billboard'];
        $this->status        = $data['status'];
        $this->vendor        = $data['vendor'];
        $this->customer      = $data['customer'];
        $this->url           = $data['url'];
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->markdown('emails.ticket_created');
    }
}
