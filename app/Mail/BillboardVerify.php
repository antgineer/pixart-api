<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class BillboardVerify extends Mailable
{
    use Queueable, SerializesModels;

    public $idBillboard;
    public $kodeBillboard;
    public $status;
    public $vendor;
    public $url;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($data)
    {
        $this->idBillboard   = $data['id_billboard'];
        $this->kodeBillboard = $data['kode_billboard'];
        $this->status        = $data['status'];
        $this->vendor        = $data['vendor'];
        $this->url           = $data['url'];
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->markdown('emails.billboard_verify');
    }
}
