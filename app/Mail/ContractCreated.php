<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class ContractCreated extends Mailable
{
    use Queueable, SerializesModels;

    public $idContract;
    public $contractDate;
    public $expiredDate;
    public $vendor;
    public $customer;
    public $url;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($data)
    {
        $this->idContract   = $data['id_contract'];
        $this->contractDate = $data['contract_date'];
        $this->expiredDate  = $data['expired_date'];
        $this->vendor       = $data['vendor'];
        $this->customer     = $data['customer'];
        $this->url          = $data['url'];
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->markdown('emails.contract_created');
    }
}
