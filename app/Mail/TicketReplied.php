<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class TicketReplied extends Mailable
{
    use Queueable, SerializesModels;

    public $idContract;
    public $kodeBillboard;
    public $sender;
    public $receiver;
    public $url;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($data)
    {
        $this->idContract    = $data['id_contract'];
        $this->kodeBillboard = $data['kode_billboard'];
        $this->sender        = $data['sender'];
        $this->receiver      = $data['receiver'];
        $this->url           = $data['url'];
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->markdown('emails.ticket_replied');
    }
}
