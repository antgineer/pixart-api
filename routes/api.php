<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// Route::middleware('auth:api')->get('/user', function (Request $request) {
//     return $request->user();
// });

//Route Login
Route::post('/login', 'AuthController@authentication');

//Refresh Token
Route::get('/refreshtoken', 'AuthController@refreshToken');

//get all billboard
Route::get('/billboard', 'BillboardController@getAllBillboard');
//get all billboard detail
Route::get('/billboard/detail', 'BillboardController@getAllBillboardDetail');
//search billboard by address
Route::get('/billboard/search', 'BillboardController@getAllBillboardMap');
//detail single billboard with contract
Route::get('/billboard/detail/single/{id}', 'BillboardController@showDetail');

//Route Provinsi
Route::get('/provinsi', 'ProvinsiController@getAllProvinsi');

//Route Kabupaten
Route::get('/kabupaten', 'KabupatenController@getAllKabupaten');
Route::get('/kabupaten/byprovinsi/{id}', 'KabupatenController@getKabupatenByProv');

//Route Kecamatan
Route::get('/kecamatan', 'KecamatanController@getAllKecamatan');
Route::get('/kecamatan/bykabupaten/{id}', 'KecamatanController@getKecamatanByKabupaten');

//Route Download File
Route::get('/download/{path}', 'FileController@download');

//Need Authentication
Route::group(['middleware' => 'jwt.auth'], function(){
    //Route Statistic
    Route::group(['prefix' => '/statistic'], function(){
        Route::get('/administrator', 'StatisticController@adminStatistic');
        Route::get('/vendor/{id}', 'StatisticController@vendorStatistic');
        Route::get('/customer/{id}', 'StatisticController@customerStatistic');
        Route::get('/contract/willexpired/{role}/{id}', 'StatisticController@countContractExpired');
        Route::get('/contract/topuser/{role}/{id}', 'StatisticController@topUser');
        Route::get('/contract/provinsi/{role}/{id}', 'StatisticController@countContractByProvinsi');
        Route::get('/contract/region/{id}', 'StatisticController@countContractByRegion');
    });

    //Route User
    Route::group(['prefix' => '/user'], function(){
        Route::get('/', 'UserController@getAllUser');
        Route::post('/', 'UserController@store');
        Route::get('/{id}', 'UserController@show');
        Route::put('/{id}', 'UserController@update');
        Route::delete('/{id}', 'UserController@destroy');
        Route::delete('/forcedelete/{id}', 'UserController@forceDelete');
    });

    //Route Provinsi
    Route::group(['prefix' => '/provinsi'], function(){
        Route::post('/', 'ProvinsiController@store');
        Route::get('/{id}', 'ProvinsiController@show');
        Route::put('/{id}', 'ProvinsiController@update');
        Route::delete('/{id}', 'ProvinsiController@destroy');
    });

    //Route Kabupaten
    Route::group(['prefix' => '/kabupaten'], function(){
        Route::post('/', 'KabupatenController@store');
        Route::get('/{id}', 'KabupatenController@show');
        Route::put('/{id}', 'KabupatenController@update');
        Route::delete('/{id}', 'KabupatenController@destroy');
    });

    //Route Kecamatan
    Route::group(['prefix' => '/kecamatan'], function(){
        Route::post('/', 'KecamatanController@store');
        Route::get('/{id}', 'KecamatanController@show');
        Route::put('/{id}', 'KecamatanController@update');
        Route::delete('/{id}', 'KecamatanController@destroy');
    });

    //Route Region
    Route::group(['prefix' => '/region'], function(){
        Route::post('/', 'RegionController@store');
        Route::get('/{id}', 'RegionController@show');
        Route::put('/{id}', 'RegionController@update');
        Route::delete('/{id}', 'RegionController@destroy');
        Route::get('/user/{id}', 'RegionController@getRegionByUser');
        Route::get('/kabupaten/{id}', 'RegionController@getKabupatenByRegion');
        Route::post('/kabupaten/{id}', 'RegionController@syncRegion');
    });

    //Route Role
    Route::group(['prefix' => '/role'], function(){
        Route::get('/', 'RoleController@getAllRole');
        Route::post('/', 'RoleController@store');
        Route::get('/{id}', 'RoleController@show');
        Route::put('/{id}', 'RoleController@update');
        Route::delete('/{id}', 'RoleController@destroy');
        Route::get('/name/{name}', 'RoleController@getRoleByName');
        Route::get('/module/{id}', 'RoleController@getModuleByRole');
        Route::post('/module/{id}', 'RoleController@syncModule');
    });

    //Route Module
    Route::group(['prefix' => '/module'], function(){
        Route::get('/', 'ModuleController@getAllModule');
        Route::post('/', 'ModuleController@store');
        Route::get('/{id}', 'ModuleController@show');
        Route::put('/{id}', 'ModuleController@update');
        Route::delete('/{id}', 'ModuleController@destroy');
    });

    //Route Billboard
    Route::group(['prefix' => '/billboard'], function(){
        //Route::get('/', 'BillboardController@getAllBillboard');
        Route::post('/', 'BillboardController@store');
        Route::get('/{id}', 'BillboardController@show');
        Route::put('/{id}', 'BillboardController@update');
        Route::delete('/{id}', 'BillboardController@destroy');
        Route::get('/detail/{id}', 'BillboardController@getAllBillboardDetailByUser');
        Route::get('/vendor/{id}', 'BillboardController@getBillboardByUser');
        Route::post('/rate', 'BillboardController@rate');
        Route::put('/viewcount/{id}', 'BillboardController@updateViewCount');
        Route::put('/verify/{id}', 'BillboardController@verify');
        Route::get('/contract/{role}/{id}', 'BillboardController@getBillboardContractByUser');
    });

    //Route Contract
    Route::group(['prefix' => '/contract'], function(){
        Route::get('/', 'ContractController@getAllContract');
        Route::post('/', 'ContractController@store');
        Route::get('/{id}', 'ContractController@show');
        Route::put('/{id}', 'ContractController@update');
        Route::delete('/{id}', 'ContractController@destroy');
        Route::post('/upload', 'ContractController@uploadPhoto');
        Route::get('/billboard/detail/{id}', 'ContractController@getContractByBillboard');
        Route::get('/{role}/{id}', 'ContractController@getContractByUser');
    });

    //Route Detail Contract
    Route::group(['prefix' => '/contract-detail'], function(){
        Route::post('/', 'BillboardContractDetailController@store');
        Route::get('/{id}', 'BillboardContractDetailController@show');
        Route::put('/{id}', 'BillboardContractDetailController@update');
        Route::delete('/{id}', 'BillboardContractDetailController@destroy');
        Route::get('/bycontract/{id}', 'BillboardContractDetailController@getDetailByContract');
    });

    //Route Ticket
    Route::group(['prefix' => '/ticket'], function(){
        Route::post('/', 'TicketController@store');
        Route::get('/{id}', 'TicketController@show');
        Route::put('/{id}', 'TicketController@update');
        Route::delete('/{id}', 'TicketController@destroy');
        Route::get('/{role}/{id}', 'TicketController@getTicketByUser');
        
        Route::group(['prefix' => '/reply'], function(){
            Route::post('/', 'TicketContentController@store');
            Route::get('/{idticket}', 'TicketContentController@show');
        });
        Route::post('close','TicketController@close');
    });

    //Route Notification
    Route::group(['prefix' => '/notification'], function(){
        Route::get('/all/{id?}', 'NotificationController@getNotification');
        Route::get('/total/{id?}', 'NotificationController@getTotalUnread');
        Route::put('/{id}/{admin?}', 'NotificationController@updateStatusRead');
    });

    //Route Event
    Route::group(['prefix' => '/event'], function(){
        Route::get('/', 'EventController@getAllEvent');
        Route::post('/', 'EventController@store');
        Route::get('/{id}', 'EventController@show');
        Route::put('/{id}', 'EventController@update');
        Route::put('/eo/{id}', 'EventController@updateEo');
        Route::delete('/{id}', 'EventController@destroy');
        Route::get('/user/{id}', 'EventController@getEventByUser');
        Route::put('/close/{id}', 'EventController@closeEvent');
    });

    //Route Ticket Event
    Route::group(['prefix' => '/ticket-event'], function(){
        Route::post('/', 'TicketEventController@store');
        Route::get('/{id}', 'TicketEventController@show');
        Route::delete('/{id}', 'TicketEventController@destroy');
        //Route::get('/user/{id}', 'TicketEventController@getTicketByUser');
        
        Route::group(['prefix' => '/reply'], function(){
            Route::post('/', 'TicketContentEventController@store');
            Route::get('/{idticket}', 'TicketContentEventController@show');
        });
        Route::put('close','TicketEventController@close');
    });

    //Route Media
    Route::group(['prefix' => '/media'], function(){
        Route::delete('/photo/{id}', 'MediaController@deletePhoto');
        Route::delete('/video/{id}', 'MediaController@deleteVideo');
    });

    //Route Change Password
    Route::put('/changepassword', 'AuthController@changePassword');
    //Route Logout
    Route::get('/logout', 'AuthController@logout');
});

Route::get('test', 'EmailController@test');