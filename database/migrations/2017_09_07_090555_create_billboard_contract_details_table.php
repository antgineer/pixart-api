<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBillboardContractDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('billboard_contract_details', function (Blueprint $table) {
            $table->increments('id');
            $table->string('tema_iklan');
            $table->string('brand');
            $table->timestamp('expired_date')->nullable();
            $table->integer('id_contract')->unsigned();
            $table->foreign('id_contract')->references('id')->on('contracts')->onUpdate('cascade')->onDelete('cascade');
            $table->integer('id_user')->unsigned()->nullable();
            $table->foreign('id_user')->references('id')->on('users')->onUpdate('cascade')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('billboard_contract_details');
    }
}
