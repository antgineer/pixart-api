<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class BillboardNotificationTrigger extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::unprepared("
            CREATE TRIGGER add_notification_billboard AFTER INSERT ON `billboards` FOR EACH ROW
            BEGIN
                INSERT INTO `notifications` (`notification_id`, `notification_type`, `id_user`, `id_sender`, `created_at`, `updated_at`)
                VALUES (NEW.id, 'billboard', NEW.id_user, NULL, NOW(), NOW());
            END
        ");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::unprepared("DROP TRIGGER `add_notification_billboard`");
    }
}
