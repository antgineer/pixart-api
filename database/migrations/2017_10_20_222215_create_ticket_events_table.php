<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTicketEventsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ticket_events', function (Blueprint $table) {
            $table->increments('id');
            $table->string('title');
            $table->char('status', 1)->default('1')->comment('0: closed, 1: open, 2:replied');
            $table->integer('id_customer')->unsigned();
            $table->foreign('id_customer')->references('id')->on('users')->onUpdate('cascade')->onDelete('cascade');
            $table->integer('id_event')->unsigned();
            $table->foreign('id_event')->references('id')->on('events')->onUpdate('cascade')->onDelete('cascade');
            $table->timestamp('closed_date')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ticket_events');
    }
}
