<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateContractsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('contracts', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamp('contract_date')->nullable();
            $table->timestamp('expired_date')->nullable();
            $table->integer('id_billboard')->unsigned();
            $table->foreign('id_billboard')->references('id')->on('billboards')->onUpdate('cascade')->onDelete('cascade');
            $table->integer('id_customer')->unsigned()->nullable();
            $table->foreign('id_customer')->references('id')->on('users')->onUpdate('cascade')->onDelete('cascade');
            $table->integer('id_vendor')->unsigned();
            $table->foreign('id_vendor')->references('id')->on('users')->onUpdate('cascade')->onDelete('cascade');
            //$table->string('tema_iklan');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('contracts');
    }
}
