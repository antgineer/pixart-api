<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class FeedbackNotificationReplyTrigger extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::unprepared("CREATE TRIGGER add_notification_feedback_reply AFTER INSERT ON `ticket_contents` FOR EACH ROW
            BEGIN
                CALL feedback_reply_notification(NEW.id_user, NEW.id_ticket);
            END
        ");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::unprepared("DROP TRIGGER `add_notification_feedback_reply`");
    }
}
