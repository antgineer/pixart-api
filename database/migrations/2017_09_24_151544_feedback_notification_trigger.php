<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class FeedbackNotificationTrigger extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::unprepared("
            CREATE TRIGGER add_notification_feedback AFTER INSERT ON `tickets` FOR EACH ROW
            BEGIN
                INSERT INTO `notifications` (`notification_id`, `notification_type`, `id_user`, `id_sender`, `created_at`, `updated_at`)
                VALUES ( (SELECT `id_contract` FROM `billboard_contract_details` WHERE id = NEW.id_detail_contract), 'ticket', (
                    SELECT `id_vendor` FROM `contracts` WHERE id = (
                        SELECT `id_contract` FROM `billboard_contract_details` WHERE id = NEW.id_detail_contract
                    )
                ), (SELECT `id_customer` FROM `contracts` WHERE id = (
                        SELECT `id_contract` FROM `billboard_contract_details` WHERE id = NEW.id_detail_contract
                    )
                ), NOW(), NOW());
            END
        ");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::unprepared("DROP TRIGGER `add_notification_feedback`");
    }
}
