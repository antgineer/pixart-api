<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ContractDetailNotificationTrigger extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::unprepared("
            CREATE TRIGGER add_notification_contract_detail AFTER INSERT ON `billboard_contract_details` FOR EACH ROW
            BEGIN
                INSERT INTO `notifications` (`notification_id`, `notification_type`, `id_user`, `id_sender`, `created_at`, `updated_at`)
                VALUES (NEW.id_contract, 'billboard_contract', (
                    SELECT `id_customer` FROM `contracts` WHERE id = NEW.id_contract LIMIT 1
                ), (
                    SELECT `id_vendor` FROM `contracts` WHERE id = NEW.id_contract LIMIT 1
                ), NOW(), NOW());
            END
        ");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::unprepared("DROP TRIGGER `add_notification_contract_detail`");
    }
}
