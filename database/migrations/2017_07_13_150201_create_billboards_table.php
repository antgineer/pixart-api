<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBillboardsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('billboards', function (Blueprint $table) {
            $table->increments('id');
            $table->string('kode_billboard')->unique();
            $table->integer('id_provinsi')->unsigned();
            $table->foreign('id_provinsi')->references('id')->on('provinsis')->onUpdate('cascade')->onDelete('cascade');
            $table->integer('id_kabupaten')->unsigned();
            $table->foreign('id_kabupaten')->references('id')->on('kabupatens')->onUpdate('cascade')->onDelete('cascade');
            $table->integer('id_kecamatan')->unsigned()->nullable();
            $table->foreign('id_kecamatan')->references('id')->on('kecamatans')->onUpdate('cascade')->onDelete('set null');
            $table->text('address');
            $table->string('longitude');
            $table->string('latitude');
            $table->string('panjang', 10);
            $table->string('lebar', 10);
            $table->string('lighting', 15);
            $table->char('side', 1);
            $table->string('arah_pandang', 10);
            $table->char('format', 1)->comment('v: vertical, h: horizontal');
            $table->string('bahan');
            $table->integer('id_user')->unsigned()->comment('vendor');
            $table->foreign('id_user')->references('id')->on('users')->onUpdate('cascade')->onDelete('cascade');
            $table->integer('harga')->unsigned();
            $table->integer('view_count')->unsigned();
            $table->char('verify', 1)->comment('0: unverified, 1: verified, 2: rejected');
            $table->text('note')->nullable();
            $table->text('reject_note')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('billboards');
    }
}
